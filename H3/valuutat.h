#pragma once

#include <istream>
#include <string>
#include <vector>

namespace otecpp_valuutat
{
    class Valuutta
    {
        public:
            Valuutta(std::string lyhenne, std::string nimi, double kurssi):
                _lyhenne(lyhenne),
                _nimi(nimi),
                _kurssi(kurssi)
            {
            }

            std::string lyhenne() const
            {
                return _lyhenne;
            }

            std::string nimi() const
            {
                return _nimi;
            }

            double kurssi() const
            {
                return _kurssi;
            }

        private:
            const std::string _lyhenne;
            const std::string _nimi;
            const double _kurssi;
    };

    std::vector<Valuutta*> *lueKurssit(std::istream &syote);
}
