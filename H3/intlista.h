#ifndef OTECPP_INTLISTA_H
#define OTECPP_INTLISTA_H

namespace otecpp_intlista
{
  class IntSolmu
  {
    int arvo;
    IntSolmu *seur;
    
    public:
    IntSolmu(int arvo, IntSolmu *seur);
    int getArvo() const;
    IntSolmu * getSeur() const;
    void setArvo(int arvo);
    void setSeur(IntSolmu *seur);
  };
  
  class IntLista
  {
    IntSolmu *paa;
    unsigned int koko;
    
    public:
    IntLista();
    IntLista(const IntLista &toinen);
    ~IntLista();
    void lisaaEteen(int arvo);
    int poistaEdesta();
    void lisaaTaakse(int arvo);
    int poistaTakaa();
    void lisaaEteen(const IntLista &toinen);
    void lisaaTaakse(const IntLista &toinen);
    void kaanna();
    void tulosta(const std::string &nimi) const;
    unsigned int getKoko() const;

    typedef unsigned int koko_t;
  };
}

#endif
