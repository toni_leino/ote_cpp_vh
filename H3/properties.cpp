#include "properties.h"

#include <cctype>
#include <sstream>

using namespace otecpp_properties;

std::string Properties::getProperty(const std::string &key) const
{
    for (std::vector<KVPair>::const_iterator it = _kvpairs.begin();
           it != _kvpairs.end(); ++it) {
        if (it->key == key) {
            return it->value;
        }
    }
    return "";
}

void Properties::list(std::ostream &out) const
{
    for (std::vector<KVPair>::const_iterator it = _kvpairs.begin();
           it != _kvpairs.end(); ++it) {
        out << it->key << "=" << it->value << "\n";
    }
}

void Properties::load(std::istream &in)
{
    std::string buf;
    while (std::getline(in, buf)) {
        std::istringstream ss(buf);
        std::string tmp;
        bool key_read = false;
        KVPair kvpair;
        char c;

        while (ss.get(c)) {
            switch (c) {
                case '\\':
                    ss.get(c);
                    tmp += c;
                    break;
                case '=':
                    if (key_read) {
                        tmp += c;
                    } else {
                        kvpair.key = tmp;
                        tmp = "";
                        key_read = true;
                    }
                    break;
                default:
                    if (!isspace(c) || key_read) {
                        tmp += c;
                    }
            }
        }
        kvpair.value = tmp;
        setProperty(kvpair.key, kvpair.value);
    }
}

std::string Properties::setProperty(const std::string &key,
                                    const std::string &value)
{
    for (std::vector<KVPair>::iterator it = _kvpairs.begin();
           it != _kvpairs.end(); ++it) {
        if (it->key == key) {
            std::string old_value = it->value;
            it->value = value;
            return old_value;
        }
    }
    KVPair tmp;
    tmp.key = key;
    tmp.value = value;
    _kvpairs.push_back(tmp);
    return "";
}

std::vector<std::string> Properties::stringPropertyNames() const
{
    std::vector<std::string> keys;
    for (std::vector<KVPair>::const_iterator it = _kvpairs.begin();
           it != _kvpairs.end(); ++it) {
        keys.push_back(it->key);
    }
    return keys;
}
