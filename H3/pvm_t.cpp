#include <iostream>
#include "pvm.h"

using namespace std;
using otecpp_pvm::Pvm;

int main()
{
  Pvm a;
  const Pvm b(26, 9, 2014);
  const Pvm c(string("3.10.2014"));
  cout << a.str() << '\n';
  cout << b.pv() << "." << b.kk() << "." << b.v() << '\n';
  string cstr = c.str();
  cout << cstr << '\n';
  a.pv(c.pv() + 1);
  a.kk(c.kk() + 1);
  a.v(c.v() + 1);
  cout << a.str() << '\n';
}
