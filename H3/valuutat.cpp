#include "valuutat.h"

#include <cstdlib>
#include <sstream>

using namespace otecpp_valuutat;

std::vector<Valuutta*> *otecpp_valuutat::lueKurssit(std::istream &syote)
{
    std::vector<Valuutta*> *kurssit = new std::vector<Valuutta*>();

    std::string buf;

    while (std::getline(syote, buf)) {
        std::istringstream ss(buf);
        char tieto[1024];
        int tietoja_luettu = 0;
        std::string lyhenne;
        std::string nimi;
        double kurssi = 0.0;
        while (ss.getline(tieto, 1024, '\t') && tietoja_luettu < 4) {
            switch (tietoja_luettu) {
                case 0:
                    lyhenne = tieto;
                    break;
                case 1:
                    nimi = tieto;
                    break;
                case 2:
                    break;
                case 3:
                    kurssi = std::atof(tieto);
                    break;
                default:
                    break;
            }
            tietoja_luettu++;
        }
        kurssit->push_back(new Valuutta(lyhenne, nimi, kurssi));
    }

    return kurssit;
}
