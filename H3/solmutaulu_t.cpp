#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "solmutaulu.h"

using namespace std;
using otecpp_solmutaulu::Solmu;
using otecpp_solmutaulu::SolmuTaulu;

namespace
{
  void tulosta(const SolmuTaulu &st, int lev)
  {
    for(Solmu *r = st.paa(); r != NULL; r = r->seurRivi())
    {
      for(Solmu *s = r; s != NULL; s = s->seurSar())
      {
        cout << setw(lev) << s->arvo();
      }
      cout << '\n';
    }
    cout << '\n';
  }
}

int main()
{
  SolmuTaulu st(3, 4);
  for(unsigned int i = 0; i < 3; ++i)
  {
    for(unsigned int j = 0; j < 4; ++j)
    {
      st.set(i, j, (i+1)*(j+1));
    }
  }
  tulosta(st, 3);
  st.lisaaRivi(0);
  st.lisaaRivi(4);
  st.lisaaRivi(2);
  st.lisaaSar(2);
  st.lisaaSar(0);
  st.lisaaSar(6);
  tulosta(st, 3);
  for(int i = 0; st.leveys() > 1; ++i)
  {
    if((i % 2) == 1)
    {
      st.poistaRivi(st.korkeus() / 2);
    }
    else
    {
      st.poistaSar(st.leveys() / 2);
    }
    tulosta(st, 3);
  }
}
