#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace otecpp_properties
{
    struct KVPair
    {
        std::string key;
        std::string value;
    };

    class Properties
    {
        public:
            std::string getProperty(const std::string &key) const;
            void list(std::ostream &out) const;
            void load(std::istream &in);
            std::string setProperty(const std::string &key,
                                    const std::string &value);
            std::vector<std::string> stringPropertyNames() const;

        private:
            std::vector<KVPair> _kvpairs;
    };
}
