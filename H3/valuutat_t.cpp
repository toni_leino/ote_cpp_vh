#include <fstream>
#include <iostream>
#include <string>
#include "valuutat.h"

using namespace std;
using otecpp_valuutat::Valuutta;
using otecpp_valuutat::lueKurssit;

int main()
{
  ifstream syote("3_2.txt");
  vector<Valuutta *> *kurssit = lueKurssit(syote);
  kurssit->push_back(new Valuutta(string("FIM"),
                        string("Finnish Markka"),0.168188));
  while(!kurssit->empty())
  {
    Valuutta *kurssi = kurssit->back();
    kurssit->pop_back();
    cout << kurssi->lyhenne() << '\n' << kurssi->nimi() << '\n'
         << 1/kurssi->kurssi() << '\n' << kurssi->kurssi() << "\n\n";
    delete kurssi;
  }
  delete kurssit;
}
