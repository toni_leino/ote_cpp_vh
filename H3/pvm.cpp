#include "pvm.h"

#include <cstdlib> // atoi
#include <sstream>

using namespace otecpp_pvm;

Pvm::Pvm():
    _pv(1),
    _kk(1),
    _v(1900)
{
}

Pvm::Pvm(int pv, int kk, int v):
    _pv(pv),
    _kk(kk),
    _v(v)
{
}

Pvm::Pvm(const std::string &pvm)
{
    std::istringstream ss(pvm);
    std::string buf;
    char c;
    int arvoja_luettu = 0;

    while (ss.get(c)) {
        if (c == '.') {
            switch (arvoja_luettu) {
                case 0:
                    _pv = atoi(buf.c_str());
                    break;
                case 1:
                    _kk = atoi(buf.c_str());
                    break;
                default:
                    // virhe?
                    break;
            }
            arvoja_luettu++;
            buf = "";
        } else {
            buf += c;
        }
    }
    _v = atoi(buf.c_str());
}

std::string Pvm::str() const
{
    std::ostringstream ss;
    ss << _pv << "." << _kk << "." << _v;
    return ss.str();
}
