#include "solmutaulu.h"

#include <cstddef>
#if DEBUG
#include <iostream>
#endif 
using namespace otecpp_solmutaulu;

SolmuTaulu::SolmuTaulu(uint korkeus, uint leveys):
    _paa(NULL),
    _leveys(0),
    _korkeus(0)
{
    if (korkeus == 0 || leveys == 0) {
        return;
    }
    for (uint i = 0; i < korkeus; ++i) {
#if DEBUG
        std::cout << "Lisätään rivi " << i << std::endl;
#endif
        lisaaRivi(i);
    }
    for (uint i = 0; i < leveys-1; ++i) {
#if DEBUG
        std::cout << "Lisätään sarake " << i << std::endl;
#endif
        lisaaSar(i);
    }
    _leveys++;
}

SolmuTaulu::~SolmuTaulu()
{
    while (_korkeus > 0) {
        poistaRivi(0);
    }
}

Solmu *SolmuTaulu::paa() const
{
    return _paa;
}

uint SolmuTaulu::leveys() const
{
    return _leveys;
}

uint SolmuTaulu::korkeus() const
{
    return _korkeus;
}

int SolmuTaulu::get(uint riv, uint sar) const
{
    if (riv >= _korkeus || sar >= _leveys) {
        return 0;
    }

    Solmu *tmp = _paa;
    while (riv--) tmp = tmp->_seurRivi;
    while (sar--) tmp = tmp->_seurSar;
    return tmp->_arvo;
}

void SolmuTaulu::set(uint riv, uint sar, int arvo)
{
    if (riv >= _korkeus || sar >= _leveys) {
        return;
    }

    Solmu *tmp = _paa;
    while (riv--) tmp = tmp->_seurRivi;
    while (sar--) tmp = tmp->_seurSar;
    tmp->_arvo = arvo;
}

void SolmuTaulu::poistaRivi(uint riv)
{
    if (riv >= _korkeus) {
        return;
    }

    Solmu *tmp = _paa;
    while (riv--) tmp = tmp->_seurRivi;
    while (tmp) {
        if (tmp == _paa) {
            _paa = tmp->_seurRivi;
        }
        Solmu *seur = tmp->_seurSar;
        if (tmp->_edelRivi) {
            tmp->_edelRivi->_seurRivi = tmp->_seurRivi;
        }
        if (tmp->_seurRivi) {
            tmp->_seurRivi->_edelRivi = tmp->_edelRivi;
        }
#if DEBUG
        std::cout << tmp << "freed" << std::endl;
#endif
        delete tmp;
        tmp = seur;
    }

    _korkeus--;
    if (_korkeus == 0) {
        _paa = NULL;
        _leveys = 0;
    }
}

void SolmuTaulu::poistaSar(uint sar)
{
    Solmu *tmp = _paa;
    while (sar--) tmp = tmp->_seurSar;
    while (tmp) {
        if (tmp == _paa) {
            _paa = tmp->_seurSar;
        }
        Solmu *seur = tmp->_seurRivi;
        if (tmp->_edelSar) {
            tmp->_edelSar->_seurSar = tmp->_seurSar;
        }
        if (tmp->_seurSar) {
            tmp->_seurSar->_edelSar = tmp->_edelSar;
        }
#if DEBUG
        std::cout << tmp << "freed" << std::endl;
#endif
        delete tmp;
        tmp = seur;
    }

    _leveys--;
    if (_leveys == 0) {
        _paa = NULL;
        _korkeus = 0;
    }
}

void SolmuTaulu::lisaaRivi(uint riv)
{
    if (riv > _korkeus) {
        return;
    }

    bool vaihda_paa = riv == 0;
    Solmu *edel = NULL, *seur = _paa;
    while (riv--) {
        edel = seur;
        seur = seur->_seurRivi;
    }
    Solmu *edelSolmu = NULL;
    do {
        Solmu *tmp = new Solmu;
#if DEBUG
        std::cout << tmp << "alloc'd" << std::endl;
#endif
        if (_paa == NULL || vaihda_paa) {
            _paa = tmp;
            vaihda_paa = false;
        }
        tmp->_arvo = 0;
        tmp->_edelRivi = edel;
        tmp->_seurRivi = seur;
        tmp->_edelSar = edelSolmu;
        if (edelSolmu) {
            edelSolmu->_seurSar = tmp;
        }
        tmp->_seurSar = NULL;
        if (edel) {
            edel->_seurRivi = tmp;
            edel = edel->_seurSar;
        }
        if (seur) {
            seur->_edelRivi = tmp;
            seur = seur->_seurSar;
        }
        edelSolmu = tmp;
    } while (edel || seur);

    _korkeus++;
}

void SolmuTaulu::lisaaSar(uint sar)
{
    if (sar > _leveys) {
        return;
    }

    bool vaihda_paa = sar == 0;
    Solmu *edel = NULL, *seur = _paa;
    while (sar--) {
        edel = seur;
        seur = seur->_seurSar;
    }
    Solmu *edelSolmu = NULL;
    do {
        Solmu *tmp = new Solmu;
#if DEBUG
        std::cout << tmp << "alloc'd" << std::endl;
#endif
        if (_paa == NULL || vaihda_paa) {
            _paa = tmp;
            vaihda_paa = false;
        }
        tmp->_arvo = 0;
        tmp->_edelSar = edel;
        tmp->_seurSar = seur;
        tmp->_edelRivi = edelSolmu;
        if (edelSolmu) {
            edelSolmu->_seurRivi = tmp;
        }
        tmp->_seurRivi = NULL;
        if (edel) {
            edel->_seurSar = tmp;
            edel = edel->_seurRivi;
        }
        if (seur) {
            seur->_edelSar = tmp;
            seur = seur->_seurRivi;
        }
        edelSolmu = tmp;
    } while (edel || seur);

    _leveys++;
}
