#include "elama.h"

#include <cstddef> // size_t

using namespace otecpp_elama;
typedef Elama::Ruutu Ruutu;

namespace
{
    // Palauttaa kaikki annetun ruudun naapurit
    std::vector<Ruutu> ruudun_naapurit(const Ruutu &ruutu)
    {
        std::vector<Ruutu> ruudut;
        for (int i = -1; i <= 1; ++i) {
            for (int j = -1; j <= 1; ++j) {
                if (i != 0 || j != 0) {
                    Ruutu tmp(ruutu.i() + i, ruutu.j() + j);
                    ruudut.push_back(tmp);
                }
            }
        }
        return ruudut;
    }
}

Elama::Elama(const std::vector<Ruutu> &alkutila):
    _elavat(alkutila),
    _sukupolvi(0)
{
}

int Elama::sukupolvi() const
{
    return _sukupolvi;
}

void Elama::simuloi(int askeleet)
{
    while (askeleet--) simuloi1();
}

std::vector<Ruutu> Elama::elavat() const
{
    return _elavat;
}

void Elama::simuloi1()
{
    std::vector<Naapuri> naapurien_lkmt = laske_naapurit();
    // Ruudut, jotka ovat elossa tämän sukupolven jälkeen
    std::vector<Ruutu> uudet_elavat;

    for (size_t i = 0; i < naapurien_lkmt.size(); ++i) {
        if (naapurien_lkmt[i].lkm == 3) {
            uudet_elavat.push_back(naapurien_lkmt[i].ruutu);
        } else if (naapurien_lkmt[i].lkm == 2) {
            // Jos ruutu oli jo ennestään elossa, se pysyy elossa.
            for (size_t j = 0; j < _elavat.size(); ++j) {
                if (_elavat[j] == naapurien_lkmt[i].ruutu) {
                    uudet_elavat.push_back(naapurien_lkmt[i].ruutu);
                    break;
                }
            }
        }
    }

    _elavat = uudet_elavat;
    _sukupolvi++;
}

std::vector<Elama::Naapuri> Elama::laske_naapurit() const
{
    std::vector<Naapuri> naapurien_lkmt;

    for (size_t i = 0; i < _elavat.size(); ++i) {
        std::vector<Ruutu> naapurit = ruudun_naapurit(_elavat[i]);
        for (size_t j = 0; j < naapurit.size(); ++j) {
            for (size_t k = 0; k < naapurien_lkmt.size(); ++k) {
                if (naapurien_lkmt[k].ruutu == naapurit[j]) {
                    naapurien_lkmt[k].lkm++;
                    goto seuraava_naapuri;
                }
            }

            {
                Naapuri tmp(naapurit[j], 1);
                naapurien_lkmt.push_back(tmp);
            }
seuraava_naapuri:
            ;
        }
    }

    return naapurien_lkmt;
}

#if DEBUG
#include <iostream>
using namespace std;

ostream& operator<<(ostream &os, vector<Ruutu> ruudut)
{
    for (size_t i = 0; i < ruudut.size(); ++i) {
        os << "(" << ruudut[i].i() << ", " << ruudut[i].j() << ") ";
    }
    return os;
}

int main()
{
    vector<Ruutu> alku;
    alku.push_back(Ruutu(0, 0));
    alku.push_back(Ruutu(0, 1));
    alku.push_back(Ruutu(0, 2));
    alku.push_back(Ruutu(0, 3));
    alku.push_back(Ruutu(1, 2));
    Elama elama(alku);
    cout << "Sukupolvi " << elama.sukupolvi() << ":";
    cout << elama.elavat() << '\n';
    for(int i = 0; i < 3; ++i)
    {
        elama.simuloi(1);
        cout << "Sukupolvi " << elama.sukupolvi() << ":";
        cout << elama.elavat() << '\n';
    }
}
#endif
