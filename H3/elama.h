#pragma once

#ifndef ELAMA_H
#define ELAMA_H

#include <vector>

namespace otecpp_elama
{
    class Elama
    {
        public:
            class Ruutu
            {
                public:
                    Ruutu(int i, int j):
                        _i(i), _j(j) { }
                    int i() const { return _i; }
                    int j() const { return _j; }
                    bool operator==(const Ruutu &ruutu2) {
                        return _i == ruutu2.i() && _j == ruutu2.j();
                    }
                private:
                    int _i;
                    int _j;
            };

            Elama(const std::vector<Ruutu> &alkutila);
            int sukupolvi() const;
            void simuloi(int askeleet);
            std::vector<Ruutu> elavat() const;

        private:
            struct Naapuri
            {
                Naapuri(const Ruutu &ruutu, int lkm):
                    ruutu(ruutu), lkm(lkm) { }
                Ruutu ruutu;
                int lkm;
            };
            // Simuloi yhden sukupolven
            void simuloi1();

            // Palauttaa kaikki ruudut, joilla on naapureita
            std::vector<Naapuri> laske_naapurit() const;

            std::vector<Ruutu> _elavat;
            int _sukupolvi;
    };
}

#endif /* end of include guard: ELAMA_H */
