#include <iostream>
#include <string>
#include "intlista.h"

using namespace std;
using otecpp_intlista::IntLista;

int main()
{
  IntLista a;
  IntLista b;
  a.tulosta(string("a"));
  for(IntLista::koko_t i = 1; i <= 5; ++i)
  {
    a.lisaaEteen(2*i);
    b.lisaaTaakse(3*i);
  }
  IntLista c(a);
  c.lisaaEteen(b);
  IntLista d(a);
  d.lisaaTaakse(b);
  d.kaanna();
  a.tulosta(string("a"));
  b.tulosta(string("b"));
  c.tulosta(string("c"));
  d.tulosta(string("d"));
  cout << "Listan \"d\" alkiot takaperin:";
  while(d.getKoko() > 0)
  {
    cout << " " << d.poistaTakaa();
  }
  cout << '\n';
}
