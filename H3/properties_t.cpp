#include "properties.h"

#include <fstream>

using namespace otecpp_properties;
using namespace std;

int main() {
    ifstream syote("syote.txt");
    Properties props;
    props.load(syote);
    props.list(cout);
    cout << props.getProperty("otecpp") << '\n';
    props.setProperty("otecpp", "C++");
    cout << props.getProperty("otecpp") << '\n';
    vector<string> avaimet = props.stringPropertyNames();
    for(vector<string>::size_type i = 0; i < avaimet.size(); ++i)
    {
    cout << "Avain[" << i << "]: " << avaimet[i] << '\n';
    }
}
