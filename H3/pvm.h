#ifndef PVM_H
#define PVM_H

#include <string>

namespace otecpp_pvm
{
    class Pvm
    {
        public:
            // Rakentajat
            Pvm();
            Pvm(int pv, int kk, int v);
            Pvm(const std::string &pvm);

            // Muut funktiot
            int pv() const { return _pv; }
            void pv(int uusi) { _pv = uusi; }
            int kk() const { return _kk; }
            void kk(int uusi) { _kk = uusi; }
            int v() const { return _v; }
            void v(int uusi) { _v = uusi; }

            std::string str() const;

        private:
            int _pv;
            int _kk;
            int _v;
    };
}

#endif /* end of include guard: PVM_H */
