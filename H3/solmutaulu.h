#pragma once

namespace otecpp_solmutaulu
{
    typedef unsigned int uint;

    struct Solmu
    {
        int arvo() const { return _arvo; }

        Solmu *edelRivi() const { return _edelRivi; }
        Solmu *seurRivi() const { return _seurRivi; }
        Solmu *edelSar()  const { return _edelSar;  }
        Solmu *seurSar()  const { return _seurSar;  }

        int _arvo;
        Solmu *_edelRivi;
        Solmu *_seurRivi;
        Solmu *_edelSar;
        Solmu *_seurSar;
    };

    class SolmuTaulu
    {
        public:
            SolmuTaulu(uint korkeus, uint leveys);
            ~SolmuTaulu();

            Solmu *paa() const;
            uint leveys() const;
            uint korkeus() const;

            int get(uint riv, uint sar) const;
            void set(uint riv, uint sar, int arvo);

            void poistaRivi(uint riv);
            void poistaSar(uint sar);

            void lisaaRivi(uint riv);
            void lisaaSar(uint sar);

        private:
            Solmu *_paa;

            uint _leveys;
            uint _korkeus;
    };
}
