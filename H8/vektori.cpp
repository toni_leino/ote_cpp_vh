#include "vektori.h"

using namespace std;
using namespace otecpp_vektori;

namespace otecpp_vektori
{
    template<typename T, size_t koko>
    const size_t otecpp_vektori::Vektori<T, koko>::n(koko);
}
