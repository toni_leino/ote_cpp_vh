#include <iostream>
#include <string>

using namespace std;

#include "lista.h"

using namespace otecpp_lista;

int main() {
    const int lkm = 5;
    const char * sanat[lkm] = {"yksi", "kaksi", "kolme", "neljä", "viisi"};
    Lista<string> lista;
    for(int i = 0; i < lkm; ++i)
    {
        lista.lisaaTaakse(sanat[i] + string("toista"));
        lista.lisaaEteen(sanat[i]);
    }
    cout << "Sanalista etuperin:\n";
    for(Lista<string>::iterator it = lista.begin(); it != lista.end(); it++)
    {
        cout << *it << " (pituus: " << it->length() << ")\n";
    }
    cout << endl;
    cout << "Sanalista takaperin:\n";
    Lista<string>::iterator it = --lista.end();
    try
    {
        while(true)
        {
            string s = *it--;
            cout << s << " (pituus: " << s.length() << ")\n";
        }
        cout << endl;
    }
    catch(out_of_range &e)
    {
        cout << "Aiheutui out_of_range-poikkeus\n";
    }
}
