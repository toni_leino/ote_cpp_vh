#ifndef PRJONO_H
#define PRJONO_H

#include <algorithm>
#include <functional>
#include <stdexcept>
#include <vector>

namespace otecpp_prjono
{
    template<typename E>
    class PriorityQueue
    {
        public:
        virtual void push(const E &e) = 0;
        virtual E peek() const throw(std::out_of_range) = 0;
        virtual E pop() throw(std::out_of_range) = 0;
    };

    template< typename T, typename Vrt = std::less<T> >
    class PrJono: public PriorityQueue<T>
    {
        public:
        void push(const T &e)
        {
            heap.push_back(e);
            std::push_heap(heap.begin(), heap.end(), vrt);
        }

        T peek() const throw(std::out_of_range)
        {
            if (heap.size() == 0) {
                throw std::out_of_range("no such element");
            }
            return heap[0];
        }

        T pop() throw(std::out_of_range)
        {
            if (heap.size() == 0) {
                throw std::length_error("no such element");
            }
            std::pop_heap(heap.begin(), heap.end(), vrt);
            T tmp = heap[heap.size()-1];
            heap.pop_back();
            return tmp;
        }

        private:
        std::vector<T> heap;
        Vrt vrt;
    };
}

#endif /* end of include guard: PRJONO_H */
