#include "sokkelo.h"

#include <string>

namespace
{
    const char TYHJA  = ' ';
    const char SEINA  = '#';
    const char PAIKKA = 'X';
    const char REITTI = 'x';
    const char KAYTY  = '*';


    const std::pair<int, int> suunnat[] = {
        std::pair<int, int>(-1,  0),
        std::pair<int, int>( 0,  1),
        std::pair<int, int>( 1,  0),
        std::pair<int, int>( 0, -1),
        std::pair<int, int>( 0,  0)
    };
}

namespace otecpp_sokkelo
{
    Sokkelo::Sokkelo(std::istream &in):
        suunta_(SUUNTA_O)
    {
        std::string buf;
        while (std::getline(in, buf)) {
            sokkelo_.push_back(std::vector<char>(buf.size(), ' '));
            for (size_t i = 0; i < buf.size(); ++i) {
                sokkelo_.back()[i] = buf[i];
                if (buf[i] == PAIKKA) {
                    pino_.push(Ruutu(sokkelo_.size() - 1, i));
                }
            }
        }
    }

    bool Sokkelo::askella(unsigned int askeleet)
    {
        if (askeleet == 0) {
            return !haku_ohi();
        }

        if (haku_ohi()) {
            return false;
        }


        bool edetty = false; // onko siirrytty eteenpäin?
        const Ruutu &r = pino_.top();
        std::pair<int, int> suunta(0, 0);
        unsigned int y, x;
        if (suunta = suunnat[(suunta_ + 1) % 4],
                y = r.first + suunta.first,
                x = r.second + suunta.second,
                sokkelo_[y][x] == TYHJA) {
            edetty = true;
            suunta_ = static_cast<Suunta>((suunta_ + 1) % 4);
        }

        else if (suunta = suunnat[suunta_],
                y = r.first + suunta.first,
                x = r.second + suunta.second,
                sokkelo_[y][x] == TYHJA) {
            edetty = true;
        }

        else if (suunta = suunnat[(suunta_ + 3) % 4],
                y = r.first + suunta.first,
                x = r.second + suunta.second,
                sokkelo_[y][x] == TYHJA) {
            edetty = true;
            suunta_ = static_cast<Suunta>((suunta_ + 3) % 4);
        }

        else if (suunta = suunnat[(suunta_ + 2) % 4],
                y = r.first + suunta.first,
                x = r.second + suunta.second,
                sokkelo_[y][x] == TYHJA) {
            edetty = true;
            suunta_ = static_cast<Suunta>((suunta_ + 2) % 4);
        }

        else {
            sokkelo_[r.first][r.second] = KAYTY;
            pino_.pop();
            if (!pino_.empty()) {
                const Ruutu &r2 = pino_.top();
                sokkelo_[r2.first][r2.second] = PAIKKA;
            }
        }

        if (edetty) {
            pino_.push(Ruutu(y, x));
            sokkelo_[r.first][r.second] = REITTI;
            sokkelo_[y][x] = PAIKKA;
        }

        return askella(--askeleet);
    }

    std::ostream& operator<<(std::ostream &out, const Sokkelo &s)
    {
        for (size_t riv = 0; riv < s.sokkelo_.size(); ++riv) {
            for (size_t sar = 0; sar < s.sokkelo_[riv].size(); ++sar) {
                out << s.sokkelo_[riv][sar];
            }
            out << '\n';
        }

        return out;
    }
}
