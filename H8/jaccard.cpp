#include "jaccard.h"

#include <algorithm>
#include <string>
#include <vector>

namespace
{
    void lue_sanat(std::istream &is, std::vector<std::string> &yhd, unsigned int k)
    {
        std::vector<std::string> tmp;
        std::string buf;

        while (is >> buf) {
            tmp.push_back(buf);
            if (tmp.size() == k) {
                std::string tmps("");
                for (size_t i = 0; i < tmp.size(); ++i) {
                    tmps += tmp[i];
                    tmps += " ";
                }
                yhd.push_back(tmps);
                tmp.erase(tmp.begin());
            }
        }
        std::sort(yhd.begin(), yhd.end());
    }
}

namespace otecpp_jaccard
{
    double jaccard(std::istream &a, std::istream &b, unsigned int k)
    {
        std::vector<std::string> sa;
        std::vector<std::string> sb;

        lue_sanat(a, sa, k);
        lue_sanat(b, sb, k);

        std::vector<std::string> leikkaus;
        std::vector<std::string> unioni;

        std::set_intersection(sa.begin(), sa.end(), sb.begin(), sb.end(),
                              std::back_inserter(leikkaus));
        std::set_union(sa.begin(), sa.end(), sb.begin(), sb.end(),
                       std::back_inserter(unioni));

        leikkaus.erase(std::unique(leikkaus.begin(), leikkaus.end()), leikkaus.end());
        unioni.erase(std::unique(unioni.begin(), unioni.end()), unioni.end());

        return (unioni.size() == 0) ? 0 : (static_cast<double>(leikkaus.size()) / unioni.size());
    }
}

#if MAIN
#include <fstream>
int main()
{
    std::ifstream a("a.txt");
    std::ifstream b("b.txt");
    std::cout << otecpp_jaccard::jaccard(a, b, 1) << std::endl;
    a.close();
    a.open("a.txt");
    b.close();
    b.open("b.txt");
    std::cout << otecpp_jaccard::jaccard(a, b, 2) << std::endl;
    a.close();
    a.open("a.txt");
    b.close();
    b.open("b.txt");
    std::cout << otecpp_jaccard::jaccard(a, b, 3) << std::endl;
}
#endif
