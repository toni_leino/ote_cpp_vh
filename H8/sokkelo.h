#ifndef SOKKELO_H
#define SOKKELO_H

#include <iostream>
#include <stack>
#include <utility>
#include <vector>

namespace otecpp_sokkelo
{
    typedef std::pair<unsigned int, unsigned int> Ruutu;

    enum Suunta
    {
        SUUNTA_Y = 0,
        SUUNTA_O = 1,
        SUUNTA_A = 2,
        SUUNTA_V = 3,
        FILE_NOT_FOUND
    };

    class Sokkelo
    {
        public:
        Sokkelo(std::istream &in);

        bool askella(unsigned int askeleet);
        std::stack<Ruutu> pino() const
        {
            return pino_;
        }

        friend std::ostream& operator<<(std::ostream &out, const Sokkelo &s);

        private:
        bool haku_ohi()
        {
            if (pino_.empty()) {
                return true;
            }

            const Ruutu &r = pino_.top();
            return r.first == 0 || r.second == 0
                || r.first == sokkelo_.size() - 1
                || r.second == sokkelo_[0].size() - 1;
        }

        Suunta suunta_;
        std::stack<Ruutu> pino_;
        std::vector< std::vector<char> > sokkelo_;
    };

    std::ostream& operator<<(std::ostream &out, const Sokkelo &s);
}

#endif /* end of include guard: SOKKELO_H */
