#ifndef RAKENNE_H
#define RAKENNE_H

#include <iostream>

namespace otecpp_rakenne
{
    template<typename Iterator>
    void rakenne(Iterator alku, Iterator loppu)
    {
        Iterator edel;
        char *p1 = NULL, *p2 = NULL, *p3 = NULL;
        for (size_t i = 0; alku != loppu; ++i) {
            if (i == 0) {
                std::cout << "[0";
                p2 = p3 = reinterpret_cast<char *>(&*alku);
                ++alku;
                continue;
            }

            p3 = reinterpret_cast<char *>(&*alku);
#if DEBUG
            std::cout << (void *) p1 << std::endl;
            std::cout << (void *) p2 << std::endl;
            std::cout << (void *) p3 << std::endl;
#endif
            if (p3 - p2 != sizeof(*alku)) {
                if (p2 - p1 == sizeof(*alku)) {
                    std::cout << "..." << i-1 << "] --> [" << i;
                } else {
                    std::cout << "] --> [" << i;
                }
            }

            edel = alku;
            if (++alku == loppu) {
                if (p2 - p1 == sizeof(*edel)) {
                    std::cout << "..." << i << "]\n";
                } else {
                    std::cout << "]\n";
                }
            }
            
            p1 = p2;
            p2 = p3;
        }
    }
}

#endif /* end of include guard: RAKENNE_H */
