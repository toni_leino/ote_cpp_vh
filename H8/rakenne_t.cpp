#include <deque>
#include <iostream>
#include <list>
#include <vector>
#include "rakenne.h"

using namespace std;
using otecpp_rakenne::rakenne;

int main()
{
  vector<double> v(10000);
  deque<char> d;
  list<int> l(10);
  for(int i = 0; i < 5000; ++i)
  {
    d.push_back(0);
    d.push_front(0);
  }
  cout << "vector-taulukon rakenne:\n";
  rakenne(v.begin(), v.end());
  cout << "deque-jonon rakenne:\n";
  rakenne(d.begin(), d.end());
  cout << "list-listan rakenne:\n";
  rakenne(l.begin(), l.end());
}
