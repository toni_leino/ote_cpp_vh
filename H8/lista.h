#ifndef OTECPP_LISTA_H
#define OTECPP_LISTA_H

#include <cassert>
#include <iostream>
#include <iterator>
#include <stack>
#include <stdexcept>
#include <vector>

namespace otecpp_lista
{
  template<typename T>
  class Lista;
}
 
namespace otecpp_lomitus
{
  template<typename T>
  void lajittele(otecpp_lista::Lista<T> &lista);
}

namespace otecpp_lista
{
  // Tarpeellista sälää
  template<typename T>
  class Lista;
  template<typename T>
  std::ostream& operator<<(std::ostream &virta, const Lista<T> &lista);

  template<typename T>
  class Solmu
  {
    T arvo;
    Solmu<T> *seur;
    
    public:
    Solmu<T>(T arvo, Solmu<T> *seur);
    T& getArvo();
    Solmu<T> * getSeur() const;
    void setArvo(T arvo);
    void setSeur(Solmu<T> *seur);
  };

  template<typename T>
  class Lista;

  template<typename T>
  class ListaIteraattori: public std::iterator<std::bidirectional_iterator_tag, T>
  {
    public:
    ListaIteraattori(Lista<T> &lista, Solmu<T> *solmu):
      lista_(lista),
      nyk_(solmu)
    {
    }

    ListaIteraattori<T>& operator++()
    {
      if (!nyk_) {
        throw std::out_of_range("ERROR");
      } else {
        nyk_ = nyk_->getSeur();
        return *this;
      }
    }

    ListaIteraattori<T> operator++(int)
    {
      ListaIteraattori<T> tmp = *this;
      ++*this;
      return tmp;
    }

    ListaIteraattori<T>& operator--()
    {
      if (nyk_ == lista_.paa) {
        throw std::out_of_range("ERROR");
      } else for (Solmu<T> *tmp = lista_.paa; ; tmp = tmp->getSeur()) {
        if (tmp->getSeur() == nyk_) {
          nyk_ = tmp;
          return *this;
        }
      }
      assert(false);
    }

    ListaIteraattori<T> operator--(int)
    {
      ListaIteraattori<T> tmp = *this;
      --*this;
      return tmp;
    }

    T& operator*()
    {
      if (!nyk_) {
        throw std::out_of_range("ERROR");
      } else {
        return nyk_->getArvo();
      }
    }

    T* operator->()
    {
      if (!nyk_) {
        throw std::out_of_range("ERROR");
      } else {
        return &nyk_->getArvo();
      }
    }

    bool operator==(const ListaIteraattori<T> &toinen)
    {
      return lista_.paa == toinen.lista_.paa && nyk_ == toinen.nyk_;
    }

    bool operator!=(const ListaIteraattori<T> &toinen)
    {
      return !(*this == toinen);
    }

    private:
    Lista<T> &lista_;
    Solmu<T> *nyk_;
  };
  
  template<typename T>
  class Lista
  {
    Solmu<T> *paa;
    unsigned int koko;
    
    public:
    Lista<T>();
    Lista<T>(const Lista<T> &toinen);
    ~Lista<T>();
    void lisaaEteen(T arvo);
    T poistaEdesta();
    void lisaaTaakse(T arvo);
    T poistaTakaa();
    void lisaaEteen(const Lista<T> &toinen);
    void lisaaTaakse(const Lista<T> &toinen);
    void kaanna();
    void tulosta(const std::string &nimi) const;
    unsigned int getKoko() const;

    Lista<T>& operator=(const Lista<T> &toinen);
    Lista<T>& operator+=(const Lista<T> &toinen);
    Lista<T> operator+(const Lista<T> &toinen) const;
    operator std::vector<T>() const;

    ListaIteraattori<T> begin();
    ListaIteraattori<T> end();

    friend std::ostream& operator<< <T>(std::ostream &virta, const Lista &lista);
    friend void otecpp_lomitus::lajittele<T>(Lista &lista);

    friend class ListaIteraattori<T>;

    typedef unsigned int koko_t;
    typedef ListaIteraattori<T> iterator;
  };
}

#endif
