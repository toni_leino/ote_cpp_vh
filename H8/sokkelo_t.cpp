#include <fstream>
#include <iostream>
#include <stack>
#include <utility>  // pair
#include "sokkelo.h"

using namespace std;
using otecpp_sokkelo::Sokkelo;

typedef stack<pair<unsigned int, unsigned int> > hakupino;

int main()
{
  ifstream td("a.txt");
  Sokkelo s(td);
  cout << s << '\n';
  s.askella(5);
  cout << s << '\n';
  for(hakupino hp = s.pino(); !hp.empty(); hp.pop())
  {
    cout << "(" << hp.top().first << ", " << hp.top().second << ")";
  }
  cout << "\n\n";
  s.askella(12);
  cout << s << '\n';
  s.askella(13);
  cout << s << '\n';
  while(s.askella(1))
    ;
  cout << s << '\n';
}
