#include <iostream>
#include "prjono.h"

using namespace std;
using namespace otecpp_prjono;

int main()
{
    int it[5] = {2, 9, 0, 4, 6};
    double dt[5] = {-5.5, 22.8, 7.0, 1.1, 13.8};
    PrJono<int, greater<int> > ipj;
    PrJono<double> dpj;
    PriorityQueue<int> &ipq = ipj;
    PriorityQueue<double> &dpq = dpj;
    for(int i = 0; i < 5; ++i)
    {
        ipq.push(it[i]);
        dpq.push(dt[i]);
    }
    cout << "Prioriteetiltaan suurimmat: " << ipq.peek() << " ja " <<  dpq.peek() << '\n';
    for(int i = 0; i < 5; ++i)
    {
        cout << "Poistin alkiot: " << ipq.pop() << " ja " << dpq.pop() << '\n';
    }
    try
    {
        cout << ipq.peek() << '\n';
    }
    catch(out_of_range &e)
    {
        cout << "Jono oli jo tyhjä\n";
    }
}
