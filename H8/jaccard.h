#ifndef JACCARD_H
#define JACCARD_H

#include <iostream>

namespace otecpp_jaccard
{
    double jaccard(std::istream &a, std::istream &b, unsigned int k = 1);
}

#endif /* end of include guard: JACCARD_H */
