#ifndef VEKTORI_H
#define VEKTORI_H

#include <algorithm>
#include <cmath>
#include <exception>
#include <ostream>
#include <string>
#include <sstream>

namespace otecpp_vektori
{
    class IllegalIndex: public std::exception
    {
        public:
        IllegalIndex(const std::string &msg = ""):
            msg_(std::string("Illegal index: ") + msg)
        {
        }
        ~IllegalIndex() throw()
        {
        }
        const char *what() const throw()
        {
            return msg_.c_str();
        }

        private:
        std::string msg_;
    };

    template<typename T, size_t koko>
    class Vektori
    {
        public:
        static const size_t n;
        typedef T * iterator;
        typedef const T * const_iterator;

        T& operator[](size_t i) throw(IllegalIndex)
        {
            if (i >= koko) {
                std::ostringstream ss;
                ss << i;
                throw IllegalIndex(ss.str());
            } else {
                return alkiot[i];
            }
        }
        const T& operator[](size_t i) const throw(IllegalIndex)
        {
            if (i >= koko) {
                std::ostringstream ss;
                ss << i;
                throw IllegalIndex(ss.str());
            } else {
                return alkiot[i];
            }
        }

        template <typename U>
        Vektori operator+(const Vektori<U, koko> &b) const
        {
            Vektori<T, koko> tmp;
            for (size_t i = 0; i < koko; ++i) {
                tmp.alkiot[i] = alkiot[i] + b.alkiot[i];
            }
            return tmp;
        }
        template <typename U>
        Vektori operator-(const Vektori<U, koko> &b) const
        {
            Vektori<T, koko> tmp;
            for (size_t i = 0; i < koko; ++i) {
                tmp.alkiot[i] = alkiot[i] - b.alkiot[i];
            }
            return tmp;
        }
        template <typename U>
        double pisteTulo(const Vektori<U, koko> &b) const
        {
            double summa = 0;
            for (size_t i = 0; i < koko; ++i) {
                summa += alkiot[i] * b.alkiot[i];
            }
            return summa;
        }

        double pituus() const
        {
            double pit = 0;
            for (size_t i = 0; i < koko; ++i) {
                pit += alkiot[i] * alkiot[i];
            }
            return sqrt(pit);
        }

        iterator begin()
        {
            return alkiot;
        }

        const_iterator begin() const
        {
            return alkiot;
        }

        iterator end()
        {
            return alkiot + koko;
        }

        const_iterator end() const
        {
            return alkiot + koko;
        }

        private:
        T alkiot[koko];
    };

    template<typename T, size_t koko>
    std::ostream& operator<<(std::ostream &out, const Vektori<T, koko> &v)
    {
        out << "[";
        for (size_t i = 0; i < koko; ++i) {
            out << v[i] << ((i != koko-1) ? ", " : "") ;
        }
        out << "]";
        return out;
    }
}

#endif /* end of include guard: VEKTORI_H */
