#include <iostream>
#include <string>

#include "lista.cpp"

using namespace std;

namespace otecpp_lista
{
  template class ListaIteraattori<string>;
  template class Lista<string>;
  template ostream & operator<<(ostream &virta, const Lista<string> &lista);
}
