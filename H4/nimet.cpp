#include "nimet.h"

using namespace otecpp_nimet;

namespace otecpp_nimet
{
    std::vector<Nimet::NimiData> Nimet::tilasto_;
    Nimet::koko_t Nimet::kaikki_;
    Nimet::koko_t Nimet::nyt_;

    Nimet::NimiData::NimiData(const std::string &nimi, koko_t kaikki, koko_t nyt):
        nimi_(nimi),
        kaikki_(kaikki),
        nyt_(nyt)
    {
    }

    Nimet::Nimet(const std::string &nimi):
        nimi_(nimi)
    {
        kaikki_++;
        nyt_++;
        lisaa_nimi();
    }

    Nimet::Nimet(const Nimet &b):
        nimi_(b.nimi())
    {
        kaikki_++;
        nyt_++;
        lisaa_nimi();
    }

    Nimet::~Nimet()
    {
        nyt_--;
        for (size_t i = 0; i < tilasto_.size(); ++i) {
            if (tilasto_[i].nimi() == nimi_) {
                tilasto_[i].nyt(tilasto_[i].nyt() - 1);
                return;
            }
        }
    }

    Nimet& Nimet::operator=(const Nimet &b)
    {
        std::string vanha = nimi_;
        nimi_ = b.nimi();
        for (size_t i = 0; i < tilasto_.size(); ++i) {
            if (tilasto_[i].nimi() == vanha) {
                tilasto_[i].nyt(tilasto_[i].nyt() - 1);
                break;
            }
        }
        lisaa_nimi();
        return *this;
    }

    std::string Nimet::nimi() const
    {
        return nimi_;
    }

    void Nimet::lisaa_nimi()
    {
        for (size_t i = 0; i < tilasto_.size(); ++i) {
            if (tilasto_[i].nimi() == nimi_) {
                tilasto_[i].kaikki(tilasto_[i].kaikki() + 1);
                tilasto_[i].nyt(tilasto_[i].nyt() + 1);
                return;
            }
        }
        for (size_t i = 0; i < tilasto_.size(); ++i) {
            if (tilasto_[i].nimi() > nimi_) {
                tilasto_.insert(tilasto_.begin() + i, NimiData(nimi_, 1, 1));
                return;
            }
        }
        tilasto_.push_back(NimiData(nimi_, 1, 1));
    }

    std::ostream& operator<<(std::ostream &out, const Nimet::NimiData &nd)
    {
        out << nd.nimi() << " " << nd.kaikki() << " " << nd.nyt();
        return out;
    }

    std::ostream& operator<<(std::ostream &out, const std::vector<Nimet::NimiData> &ndv)
    {
        out << Nimet::kaikki() << " " << Nimet::nyt() << std::endl;
        for (size_t i = 0; i < ndv.size(); ++i) {
            out << ndv[i] << std::endl;
        }
        return out;
    }
}
