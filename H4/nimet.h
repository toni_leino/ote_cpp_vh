#ifndef NIMET_H
#define NIMET_H

#include <cstddef>
#include <iostream>
#include <string>
#include <vector>

namespace otecpp_nimet
{
    class Nimet
    {
        public:
            typedef size_t koko_t;

            class NimiData
            {
                public:
                    NimiData(const std::string &nimi = "",
                             koko_t kaikki = 0, koko_t nyt = 0);
                    const std::string& nimi() const
                    {
                        return nimi_;
                    }
                    koko_t kaikki() const
                    {
                        return kaikki_;
                    }
                    void kaikki(koko_t kaikki)
                    {
                        kaikki_ = kaikki;
                    }
                    koko_t nyt() const
                    {
                        return nyt_;
                    }
                    void nyt(koko_t nyt)
                    {
                        nyt_ = nyt;
                    }

                private:
                    std::string nimi_;
                    koko_t kaikki_;
                    koko_t nyt_;
            };

            static koko_t kaikki()
            {
                return kaikki_;
            }

            static koko_t nyt()
            {
                return nyt_;
            }

            static NimiData tilasto(const std::string &nimi)
            {
                for (size_t i = 0; i < tilasto_.size(); ++i) {
                    if (tilasto_[i].nimi() == nimi) {
                        return tilasto_[i];
                    }
                }
                return Nimet::NimiData(nimi);
            }

            static std::vector<NimiData> tilasto()
            {
                return tilasto_;
            }

            Nimet(const std::string &nimi);
            Nimet(const Nimet &b);
            ~Nimet();
            Nimet& operator=(const Nimet &b);
            std::string nimi() const;

        private:
            static koko_t kaikki_;
            static koko_t nyt_;
            static std::vector<NimiData> tilasto_;
            std::string nimi_;

            // Lisätään nykyinen nimi tilastoon
            void lisaa_nimi();
    };

    std::ostream& operator<<(std::ostream &out, const Nimet::NimiData &nd);
    std::ostream& operator<<(std::ostream &out, const std::vector<Nimet::NimiData> &ndv);
}

#endif /* end of include guard: NIMET_H */
