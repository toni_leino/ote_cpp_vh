#include "pascal.h"

using otecpp_isounsigned::IsoUnsigned;

namespace otecpp_pascal
{
    Pascal::Pascal(unsigned int n)
    {
        this->n(n);
    }

    unsigned int Pascal::n() const
    {
        return rivit_.size() - 1;
    }

    void Pascal::n(unsigned int n)
    {
        if (n+1 < rivit_.size()) {
            rivit_.resize(n+1);
            return;
        } else if (n+1 == rivit_.size()) {
            return;
        }

        for (unsigned int i = rivit_.size(); i <= n; ++i) {
            rivit_.push_back(std::vector<IsoUnsigned>());
            for (size_t j = 0; j <= i; ++j) {
                if (j == 0 || j == i) {
                    rivit_[i].push_back(1);
                } else {
                    rivit_[i].push_back(rivit_[i-1][j-1] + rivit_[i-1][j]);
                }
            }
        }
    }

    IsoUnsigned Pascal::operator()(unsigned int n, unsigned int k)
    {
        if (k > n) {
            return IsoUnsigned(0);
        }

        if (rivit_.size() < n+1) {
            this->n(n);
        }

        return rivit_[n][k];
    }

    std::ostream& operator<<(std::ostream &virta, const Pascal &p)
    {
        for (size_t i = 0; i < p.rivit_.size(); ++i) {
            for (size_t j = 0; j <= i; ++j) {
                virta << p.rivit_[i][j] << (j != i ? " " : "");
            }
            virta << '\n';
        }
        return virta;
    }
}
