#ifndef ISOUNSIGNED_H
#define ISOUNSIGNED_H

#include <iostream>
#include <sstream>
#include <string>

namespace otecpp_isounsigned
{
    class IsoUnsigned
    {
        std::string luku;

        public:
        IsoUnsigned(unsigned long arvo = 0)
        {
            std::ostringstream oss;
            oss << arvo;
            luku = oss.str();
        }

        IsoUnsigned& operator++();
        IsoUnsigned operator++(int);
        IsoUnsigned& operator +=(const IsoUnsigned &b);
        IsoUnsigned& operator -=(const IsoUnsigned &b);

        bool operator<(const IsoUnsigned &b) const;
        bool operator>(const IsoUnsigned &b) const;
        bool operator==(const IsoUnsigned &b) const;
        bool operator!=(const IsoUnsigned &b) const;

        friend IsoUnsigned operator+(const IsoUnsigned &eka, const IsoUnsigned &toinen);
        friend IsoUnsigned operator-(const IsoUnsigned &eka, const IsoUnsigned &toinen);

        friend std::ostream & operator<<(std::ostream &out, const IsoUnsigned &arvo);
        friend std::istream & operator>>(std::istream &in, IsoUnsigned &arvo);
    };
}

#endif /* end of include guard: ISOUNSIGNED_H */
