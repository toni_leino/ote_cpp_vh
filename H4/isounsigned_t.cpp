#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include "isounsigned.h"

using namespace std;
using namespace otecpp_isounsigned;

int main()
{
    IsoUnsigned a(1);
    for(int i = 1; i <= 11; ++i)
    { // Asettaa a:n arvoksi 2 potenssiin 11.
        a += a;
    }
    IsoUnsigned b(1);
    while(b < a) // Asettaa a:n arvoksi pienimmän 3:n potenssin,
        { // joka ei ole pienempi kuin 2 potenssiin 11.
            b += (b + b);
        }
    IsoUnsigned c(1);
    const IsoUnsigned nolla; // IsoUnsigned-arvo 0.
    cout << setw(7) << a << setw(7) << b << setw(7) << c << '\n';
    while(!(a == nolla) || (b != nolla))
    { // Vähennetään c suuremmasta, kunnes kumpikin on 0.
        if(a > b)
        {
            a = a - c;
        }
        else if(a < b)
        {
            b -= c;
        }
        else  // Yhtäsuuria? Vähennetään molemmista.
        {
            a -= c;
            b = b - c;
        }
        // Kasvatetaan c:n arvoa, jotta ei mene koko ikää.
        c += (c += (c + c)); // Mitä c:n arvolle käy?
        cout << setw(7) << a << setw(7) << b << setw(7) << c << '\n';
    }
}
