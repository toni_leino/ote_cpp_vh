#ifndef PASCAL_H
#define PASCAL_H

#include <iostream>
#include <vector>

#include "isounsigned.h"

namespace otecpp_pascal
{
    class Pascal
    {
        public:
            Pascal(unsigned int n = 0);

            unsigned int n() const;
            void n(unsigned int n);

            otecpp_isounsigned::IsoUnsigned operator()
                    (unsigned int n, unsigned int k);
            
            friend std::ostream& operator<<
                    (std::ostream &virta, const Pascal &p);

        private:
            std::vector< std::vector<otecpp_isounsigned::IsoUnsigned> > rivit_;
    };
}

#endif /* end of include guard: PASCAL_H */
