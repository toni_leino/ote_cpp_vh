#include <iostream>
#include <vector>
#include "intlista.h"

using namespace std;
using namespace otecpp_intlista;

int main()
{
  IntLista a;
  IntLista b;
  IntLista c;
  for(int i = 1; i <= 3; ++i)
  {
    a.lisaaEteen(i);
    cout << "a: " << a << '\n';
    b.lisaaEteen(5*i);
    cout << "b: " << b << '\n';
    c += (a + b);
    cout << "c: " << c << '\n';
  }
  vector<int> d = (a + a);
  a += a;
  cout << "d:";
  for(vector<int>::size_type i = 0; i < d.size(); ++i)
  {
    cout << " " << d[i];
  }
  cout << "\na: " << a << '\n';
  a = b = c = (b + b);
  cout << "a: " << a << '\n';
}
