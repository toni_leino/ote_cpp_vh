#ifndef BITIT_H
#define BITIT_H

#include <ostream>
#include <string>
#include <vector>

namespace otecpp_bitit
{
    class BittiVektori
    {
        public:
            typedef std::vector<bool>::size_type koko_t;

            // Rakentajat
            BittiVektori(unsigned long bitit = 0);
            BittiVektori(koko_t n, bool arvo);
            explicit BittiVektori(const std::string &bv);

            koko_t koko() const;
            bool operator[](koko_t i) const;
            operator unsigned long() const;

            void set(koko_t i, bool arvo);

            BittiVektori& operator&=(const BittiVektori &b);
            BittiVektori& operator|=(const BittiVektori &b);
            BittiVektori& operator^=(const BittiVektori &b);

        private:
            std::vector<bool> bitit_;

            // Geneerinen ratkaisu operaattoreita varten.
            BittiVektori& logical_op
                (const BittiVektori &b, bool (*fn)(bool, bool));
    };

    BittiVektori operator&(const BittiVektori &a, const BittiVektori &b);
    BittiVektori operator|(const BittiVektori &a, const BittiVektori &b);
    BittiVektori operator^(const BittiVektori &a, const BittiVektori &b);
    BittiVektori operator~(const BittiVektori &a);

    std::ostream& operator<<(std::ostream &virta, const BittiVektori &bv);
}

#endif /* end of include guard: BITIT_H */
