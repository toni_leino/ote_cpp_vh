#include <iostream>

#include "nimet.h"

using namespace otecpp_nimet;
using namespace std;
int main()
{
    cout << Nimet::tilasto();
    const char * nimia[5] = {"Matti", "Ilona", "Maija", "Esko", "Anna"};
    vector<Nimet> nv;
    for(int i = 0; i < 8; ++i)
    {
        string nimi(nimia[i%5]);
        nv.push_back(Nimet(nimi));
        cout << Nimet::tilasto(nimi) << '\n';
    }
    cout << Nimet::tilasto();
    for(vector<Nimet>::size_type i = 0; i < nv.size(); ++i)
    {
        if(nv[i].nimi() == "Matti")
        {
            nv[i] = Nimet("Maija");
        }
    }
    cout << Nimet::tilasto();
}
