#include "bitit.h"

#include <climits>
#include <algorithm>

using otecpp_bitit::BittiVektori;

BittiVektori::BittiVektori(unsigned long bitit)
{
    for (koko_t i = 0; i < sizeof(bitit) * CHAR_BIT; ++i) {
        // (x & 1) == x:n viimeinen bitti
        bitit_.push_back(bitit & 1);
        bitit >>= 1;
    }
}

BittiVektori::BittiVektori(koko_t n, bool arvo):
    bitit_(n, arvo)
{
}

BittiVektori::BittiVektori(const std::string &bv)
{
    for (size_t i = bv.size(); i-- > 0;) {
        // Hyödynnetään implisiittistä tyyppimuunnosta; 0 == false, 1 == true
        bitit_.push_back(bv[i]-'0');
    }
}

BittiVektori::koko_t BittiVektori::koko() const
{
    return bitit_.size();
}

bool BittiVektori::operator[](koko_t i) const
{
    return bitit_[i];
}

BittiVektori::operator unsigned long() const
{
    static const size_t ULONG_BITIT = sizeof(unsigned long)*CHAR_BIT;

    unsigned long tmp = 0;
    for (koko_t i = 0; i < std::min(koko(), ULONG_BITIT); ++i) {
        tmp <<= 1;
        tmp |= bitit_[std::min(koko(), ULONG_BITIT)-i-1];
    }
    return tmp;
}

void BittiVektori::set(koko_t i, bool arvo)
{
    bitit_[i] = arvo;
}

/*
 * Loogiset operaatiot.
 *
 * Nämä on toteutettu geneerisesti hyödyntäen funktio-osoittimia. Tämä
 * mahdollistaa esimerkiksi uusien operaattorien helpon lisäämisen.
 */

namespace
{
    bool and_op(bool a, bool b)
    {
        return a && b;
    }

    bool or_op(bool a, bool b)
    {
        return a || b;
    }

    bool xor_op(bool a, bool b)
    {
        return a ^ b;
    }
}

BittiVektori& BittiVektori::logical_op
        (const BittiVektori &b, bool (*fn)(bool, bool))
{
    bitit_.resize(std::min(bitit_.size(), b.koko()));
    for (koko_t i = 0; i < bitit_.size(); ++i) {
        bitit_[i] = fn(bitit_[i], b[i]);
    }
    return *this;
}

BittiVektori& BittiVektori::operator&=(const BittiVektori &b)
{
    return logical_op(b, and_op);
}

BittiVektori& BittiVektori::operator|=(const BittiVektori &b)
{
    return logical_op(b, or_op);
}

BittiVektori& BittiVektori::operator^=(const BittiVektori &b)
{
    return logical_op(b, xor_op);
}

namespace
{
    // Luo vektorin, jossa tulos[i] = fn(a[i], b[i]).
    BittiVektori luo_vektori(const BittiVektori &a, const BittiVektori &b,
                             bool (*fn)(bool, bool))
    {
        BittiVektori tulos(std::min(a.koko(), b.koko()), false);
        for (BittiVektori::koko_t i = 0; i < a.koko() && i < b.koko(); ++i) {
            tulos.set(i, fn(a[i], b[i]));
        }
        return tulos;
    }
}

namespace otecpp_bitit
{
    BittiVektori operator&(const BittiVektori &a, const BittiVektori &b)
    {
        return luo_vektori(a, b, and_op);
    }

    BittiVektori operator|(const BittiVektori &a, const BittiVektori &b)
    {
        return luo_vektori(a, b, or_op);
    }

    BittiVektori operator^(const BittiVektori &a, const BittiVektori &b)
    {
        return luo_vektori(a, b, xor_op);
    }

    // Tätä ei voi toteuttaa geneerisesti, koska NOT ei ole binäärioperaattori.
    BittiVektori operator~(const BittiVektori &a)
    {
        BittiVektori tulos(a.koko(), false);
        for (BittiVektori::koko_t i = 0; i < a.koko(); ++i) {
            tulos.set(i, !a[i]);
        }
        return tulos;
    }

    std::ostream& operator<<(std::ostream &virta, const BittiVektori &bv)
    {
        for (BittiVektori::koko_t i = bv.koko(); i-- > 0;) {
            virta << static_cast<char>('0' + bv[i]);
        }
        return virta;
    }
}
