#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

#include "isounsigned.h"

using namespace std;

namespace otecpp_isounsigned
{
    IsoUnsigned& IsoUnsigned::operator++()
    {
        for (size_t n = luku.size(); n-- > 0;) {
            if (++(luku[n]) <= '9')
                goto ret;
            else {
                luku[n] = '0';
            }
        }

        luku.insert(0, 1, '1');
ret:
        return *this;
    }

    IsoUnsigned IsoUnsigned::operator++(int)
    {
        IsoUnsigned tmp = *this;
        operator++();
        return tmp;
    }

    IsoUnsigned& IsoUnsigned::operator+=(const IsoUnsigned &b)
    {
        IsoUnsigned tmp = *this + b;
        luku = tmp.luku;
        return *this;
    }

    IsoUnsigned& IsoUnsigned::operator-=(const IsoUnsigned &b)
    {
        IsoUnsigned tmp = *this - b;
        luku = tmp.luku;
        return *this;
    }

    bool IsoUnsigned::operator<(const IsoUnsigned &b) const
    {
        if (luku.size() != b.luku.size()) {
            return luku.size() < b.luku.size();
        } else {
            return luku < b.luku;
        }
    }

    bool IsoUnsigned::operator>(const IsoUnsigned &b) const
    {
        return b < *this;
    }

    bool IsoUnsigned::operator==(const IsoUnsigned &b) const
    {
        return luku == b.luku;
    }

    bool IsoUnsigned::operator!=(const IsoUnsigned &b) const
    {
        return !(*this == b);
    }

    IsoUnsigned operator+(const IsoUnsigned &eka, const IsoUnsigned &toinen)
    {
        IsoUnsigned tulos;
        tulos.luku = "";
        string::size_type i = eka.luku.size();
        string::size_type j = toinen.luku.size();
        int arvo = 0;
        while((i > 0) || (j > 0))
        {
            char a = (i > 0) ? eka.luku[--i] : '0';
            char b = (j > 0) ? toinen.luku[--j] : '0';
            arvo = arvo + (a -'0') + (b -'0');
            tulos.luku.insert(0, 1, (arvo % 10) + '0');
            arvo = arvo/10;
        }
        if(arvo > 0)
        {
            tulos.luku.insert(0, 1, (arvo % 10) + '0');
        }
        return tulos;
    }

    IsoUnsigned operator-(const IsoUnsigned &eka, const IsoUnsigned &toinen)
    {
        if (eka < toinen) {
            IsoUnsigned tulos;
            tulos.luku = "0";
            return tulos;
        }
        IsoUnsigned tulos;
        tulos.luku = eka.luku;
        int vahenna_yksi = 0;
        for (size_t i = eka.luku.size(), j = toinen.luku.size();
             --i, j-- > 0;) {
            int tmp = tulos.luku[i] - toinen.luku[j] - vahenna_yksi;
            vahenna_yksi = 0;
            if (tmp < 0) {
                vahenna_yksi = 1;
                tmp += 10;
            }
            tulos.luku[i] = tmp + '0';
        }
        for (size_t i = eka.luku.size() - toinen.luku.size();
             vahenna_yksi && i-- > 0;) {
            tulos.luku[i] -= 1;
            vahenna_yksi = false;
            if (tulos.luku[i] < '0') {
                tulos.luku[i] += 10;
                vahenna_yksi = 1;
            }
        }
        while (tulos.luku[0] == '0') {
            tulos.luku.erase(0, 1);
        }
        return tulos;
    }

    ostream & operator<<(ostream &out, const IsoUnsigned &arvo)
    {
        out << arvo.luku;
        return out;
    }
    
    istream & operator>>(istream &in, IsoUnsigned &arvo)
    {
        while (isspace(in.peek())) {
            in.get();
        }

        arvo.luku = "";
        if (!isdigit(in.peek())) {
            in.setstate(ios_base::failbit);
            goto end;
        }

        char c;
        while (isdigit(c = in.peek())) {
            arvo.luku += c;
            in.get();
        }

end:
        return in;
    }
}
