#include <cstddef>
#include <climits>
#include <iostream>

#include "intlista.h"

using namespace std;

namespace otecpp_intlista
{
  IntSolmu::IntSolmu(int arvo, IntSolmu *seur)
  {
    this->arvo = arvo;
    this->seur = seur;
  }
  
  int IntSolmu::getArvo() const
  {
    return arvo;
  }
  
  IntSolmu * IntSolmu::getSeur() const
  {
    return seur;
  }

  void IntSolmu::setArvo(int arvo)
  {
    this->arvo = arvo;
  }

  void IntSolmu::setSeur(IntSolmu *seur)
  {
    this->seur = seur;
  }
  
  IntLista::IntLista()
  {
    koko = 0;
    paa = NULL;
  }

  IntLista::IntLista(const IntLista &toinen)
  {
    koko = toinen.getKoko();

    IntSolmu *nyk = toinen.paa;
    IntSolmu *edel = NULL;
    while (nyk) {
      IntSolmu *tmp = new IntSolmu(nyk->getArvo(), NULL);
      if (nyk == toinen.paa) {
        paa = tmp;
      }
      if (edel) {
        edel->setSeur(tmp);
      }
      nyk = nyk->getSeur();
      edel = tmp;
    }
  }

  IntLista::~IntLista()
  {
    IntSolmu *nyk = paa;
    IntSolmu *seur = NULL;
    while (nyk) {
      seur = nyk->getSeur();
      delete nyk;
      nyk = seur;
    }
  }
  
  void IntLista::lisaaEteen(int arvo)
  {
    paa = new IntSolmu(arvo, paa);
    koko += 1;
  }
    
  int IntLista::poistaEdesta()
  {
    int tulos = 0;
    if(koko > 0)
    {
      IntSolmu *vanha = paa;
      paa = paa->getSeur();
      koko -= 1;
      tulos = vanha->getArvo();
      delete vanha;
    }
    return tulos;
  }

  void IntLista::lisaaTaakse(int arvo) {
    if (paa == NULL) {
      lisaaEteen(arvo);
      return;
    }
    IntSolmu *nyk = paa;
    while (nyk->getSeur()) {
      nyk = nyk->getSeur();
    }
    IntSolmu *tmp = new IntSolmu(arvo, NULL);
    nyk->setSeur(tmp);
    koko++;
  }

  int IntLista::poistaTakaa()
  {
    if (koko == 1) {
      int arvo = paa->getArvo();
      delete paa;
      paa = NULL;
      koko = 0;
      return arvo;
    }
    IntSolmu *nyk = paa;
    while (nyk->getSeur()->getSeur()) {
      nyk = nyk->getSeur();
    }
    int arvo = nyk->getSeur()->getArvo();
    delete nyk->getSeur();
    nyk->setSeur(NULL);
    koko--;
    return arvo;
  }

  void IntLista::lisaaEteen(const IntLista &toinen)
  {
    IntLista tmp(toinen);
    tmp.kaanna();
    
    IntSolmu *nyk = tmp.paa;
    while (nyk) {
      lisaaEteen(nyk->getArvo());
      nyk = nyk->getSeur();
    }
  }

  void IntLista::lisaaTaakse(const IntLista &toinen)
  {
    IntSolmu *edel = paa; // solmu, jonka taakse lisätään
    if (paa != NULL) {
        while (edel->getSeur()) {
            edel = edel->getSeur();
        }
    }
    IntSolmu *nyk = toinen.paa; // solmu, jota ollaan kopioimassa
    koko_t n = UINT_MAX; // maksimimäärä solmuja, jotka kopioidaan
    if (&toinen == this) {
      n = koko;
    }
    while (nyk && n-- > 0) {
      IntSolmu *tmp = new IntSolmu(nyk->getArvo(), NULL);
      if (paa == NULL) {
          paa = tmp;
          paa->setSeur(tmp);
      } else {
          edel->setSeur(tmp);
      }
      edel = tmp;
      nyk = nyk->getSeur();
      koko++;
    }
  }

  void IntLista::kaanna()
  {
      IntSolmu *s1 = NULL;
      IntSolmu *s2 = paa;
      IntSolmu *s3 = paa->getSeur();

      while (s3 != NULL) {
          s2->setSeur(s1);
          s1 = s2;
          s2 = s3;
          s3 = s3->getSeur();
      }
      s2->setSeur(s1);
      paa = s2;
  }

  void IntLista::tulosta(const string &nimi) const
  {
    cout << "Listan \"" << nimi << "\" koko on " << koko;
    if (koko == 0) {
      cout << endl;
      return;
    }
    cout << ", alkiot:";
    IntSolmu *nyk = paa;
    while (nyk) {
      cout << " " << nyk->getArvo();
      nyk = nyk->getSeur();
    }
    cout << endl;
  }
    
  unsigned int IntLista::getKoko() const
  {
    return koko;
  }

  IntLista& IntLista::operator=(const IntLista &toinen)
  {
    if (&toinen == this) {
      return *this;
    }
    while (paa != NULL) {
      poistaEdesta();
    }
    lisaaTaakse(toinen);
    return *this;
  }

  IntLista& IntLista::operator+=(const IntLista &toinen)
  {
    lisaaTaakse(toinen);
    return *this;
  }

  IntLista IntLista::operator+(const IntLista &toinen) const
  {
    IntLista tmp(*this);
    tmp.lisaaTaakse(toinen);
    return tmp;
  }

  IntLista::operator vector<int>() const
  {
    vector<int> v;
    IntSolmu *tmp = paa;
    while (tmp) {
      v.push_back(tmp->getArvo());
      tmp = tmp->getSeur();
    }
    return v;
  }

  ostream& operator<<(ostream &virta, const IntLista &lista)
  {
    IntSolmu *tmp = lista.paa;
    while (tmp) {
      virta << tmp->getArvo();
      if ((tmp = tmp->getSeur())) {
        virta << " ";
      }
    }
    return virta;
  }
}
