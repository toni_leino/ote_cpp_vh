#include "bitit.h"
#include <iostream>
using namespace std;
using namespace otecpp_bitit;

int main()
{
    BittiVektori bv(2014); // Alustaa unsigned long-arvon biteillä.
    BittiVektori bv2(70, true); // Alustaa 70:llä 1-bitillä.
    for(BittiVektori::koko_t i = bv.koko(); i > 0; --i)
    {  // Tulostus bittijonon lopusta alkuun (vasemmalta oikealle).
        cout << bv[i-1];
    }
    cout << '\n'<< bv2 << '\n';
    cout << (BittiVektori("001110") & BittiVektori("10101")) << '\n';
    cout << (BittiVektori("01110") | BittiVektori("010101")) << '\n';
    cout << (BittiVektori("101110") ^ BittiVektori("10101")) << '\n';
    cout << ~BittiVektori("01110") << '\n';
    BittiVektori bv3("1011010101");
    cout << (bv3 &= BittiVektori("1011010101")) << '\n';
    cout << (bv3 |= BittiVektori("001010100101")) << '\n';
    cout << (bv3 ^= BittiVektori("00101100")) << '\n';
    cout << static_cast<unsigned long>(bv3) << '\n';  // unsigned long-arvoksi.
}
