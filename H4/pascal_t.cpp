#include <iostream>
#include "pascal.h"

using namespace std;
using namespace otecpp_pascal;

int main()
{
    Pascal p(2);
    cout << "Kaksi yli yhden on " << p(2, 1) << '\n';
    cout << "Viisi yli kolmen on " << p(5, 3) << '\n';
    cout << "Pascalin kolmion rivit 0..." << p.n() << " ovat:\n" << p;
    p.n(3);
    cout << "Pascalin kolmion rivit 0..." << p.n() << " ovat:\n" << p;
    cout << "100 yli 35:n on " << p(100, 35) << '\n';
}
