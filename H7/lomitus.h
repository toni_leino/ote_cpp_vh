#ifndef LOMITUS_H
#define LOMITUS_H

#include <cstddef>
#include <vector>

namespace otecpp_lomitus
{
    template<typename T>
    std::vector<T> lomita(const std::vector<T> &a, const std::vector<T> &b);

    template<typename T>
    std::vector<T*> lomita(const std::vector<T*> &a, const std::vector<T*> &b,
                           bool kopioi = false); 

    std::vector<const char *> lomita
        (const std::vector<const char *> &a, const std::vector<const char *> &b, bool kopioi = false);
}

#endif /* end of include guard: LOMITUS_H */
