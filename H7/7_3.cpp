#include "vektori.cpp"

namespace otecpp_vektori
{
    template class Vektori<int, 3>;
    template Vektori<int, 3> Vektori<int, 3>::operator+(const Vektori<int, 3> &) const;
    template Vektori<int, 3> Vektori<int, 3>::operator-(const Vektori<int, 3> &) const;
    template double Vektori<int, 3>::pisteTulo(const Vektori<int, 3> &) const;
    template ostream & operator<< <int, 3>(ostream &, const Vektori<int, 3> &);
    template class Vektori<int, 10>;
    template ostream & operator<< <int, 10>(ostream &, const Vektori<int, 10> &);
}
