#include <iostream>
#include <cstdlib>
#include <vector>
#include "lomitus.h"

using namespace std;
using otecpp_lomitus::lomita;

int main()
{
  int ia[5] = {0, 3, 4, 8, 9};
  vector<int *> a(5);
  a[0] = &ia[0]; a[1] = &ia[1]; a[2] = &ia[2]; a[3] = &ia[3]; a[4] = &ia[4];
  int ib[3] = {2, 3, 5};
  vector<int *> b(3);
  b[0] = &ib[0]; b[1] = &ib[1]; b[2] = &ib[2];
  vector<int *> ab = lomita(a, b);
  vector<int *> ab2 = lomita(a, b, true);
  for(vector<int>::size_type i = 0; i < ab.size(); ++i)
  {
    cout << " " << *ab[i] << "=" << *ab2[i];
    delete ab2[i];
  }
  cout << endl;
  vector<const char *> c(3);
  c[0] = "kaksi"; c[1] = "kolme"; c[2] = "yksi";
  vector<const char *> d(3);
  d[0] = "kuusi"; d[1] = "neljä"; d[2] = "viisi";
  vector<const char *> cd = lomita(c, d);
  vector<const char *> cd2 = lomita(c, d, true);
  for(vector<const char *>::size_type i = 0; i < cd.size(); ++i)
  {
    cout << " " << cd[i] << "=" << cd2[i];
    free(const_cast<char *>(cd2[i]));
  }
  cout << endl;
}
