#ifndef OTEPTR_H
#define OTEPTR_H

#include <cstddef>
#include <iostream>
#include <string>

namespace otecpp_oteptr
{
    class IllegalState: public std::exception
    {
        public:
            IllegalState(const std::string &msg = ""):
                msg_(std::string("Illegal state: ") + msg)
                {
                }
            ~IllegalState() throw()
            {
            }
            const char *what() const throw()
            {
                return msg_.c_str();
            }

        private:
            std::string msg_;
    };

    template<typename T, bool isArray = false>
    class OtePtr
    {
        public:
        OtePtr():
            ptr(NULL),
            counter(NULL)
        {
        }

        OtePtr(T *ptr):
            ptr(ptr),
            counter(new size_t(1))
        {
        }

        OtePtr(const OtePtr &b)
        {
            ptr = b.ptr;
            ++*(counter = b.counter);
        }

        ~OtePtr()
        {
            if (ptr && --*counter == 0) {
                if (isArray) {
                    delete[] ptr;
                } else {
                    delete ptr;
                }
                delete counter;
            }
        }

        OtePtr<T, isArray>& operator=(const OtePtr &b)
        {
            if (ptr && --*counter == 0) {
                if (isArray) {
                    delete[] ptr;
                } else {
                    delete ptr;
                }
                delete counter;
            }

            ptr = b.ptr;
            ++*(counter = b.counter);
            return *this;
        }

        T& operator[](size_t i)
        {
            if (!isArray) {
                throw IllegalState("not an array");
            } else if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return ptr[i];
            }
        }

        const T& operator[](size_t i) const
        {
            if (!isArray) {
                throw IllegalState("not an array");
            } else if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return ptr[i];
            }
        }

        T& operator*()
        {
            if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return *ptr;
            }
        }

        const T& operator*() const
        {
            if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return *ptr;
            }
        }

        T* operator->()
        {
            if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return ptr;
            }
        }

        const T* operator->() const
        {
            if (!ptr) {
                throw IllegalState("undefined value");
            } else {
                return ptr;
            }
        }

        T* get()
        {
            return ptr;
        }

        const T* get() const
        {
            return ptr;
        }

        private:
        T *ptr;
        size_t *counter;
    };
}

#endif /* end of include guard: OTEPTR_H */
