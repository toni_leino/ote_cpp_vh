#include <iostream>
#include <vector>
#include "comparable.h"
#include "luvut.h"

using namespace std;
using otecpp_comparable::Comparable;
using otecpp_comparable::ComparableBeta;
using otecpp_comparable::IllegalComparison;
using otecpp_luvut::DivideByZero;
using otecpp_luvut::Integer;
using otecpp_luvut::Murtoluku;

int main()
{
  ComparableBeta * ct[2] = {new Integer(1), new Integer(3)};
  int it[3] = {0, 2, -1};
  for(unsigned int i = 0; i < 2; i++)
  {
    for(unsigned int j = 0; j < 2; j++)
    {
      try
      {
        cout << "Indeksit " << i << " vs " << j << ":\n";
        cout << ct[i]->compareTo(ct[j]) << '\n';
      }
      catch(IllegalComparison &e)
      {
        cout << e.what() << '\n';
      }
      try
      {
        Murtoluku *ml = new Murtoluku(it[i], it[j]);
        cout << ml << '\n';
        delete ml;
      }
      catch(DivideByZero &e)
      {
        cout << e.what() << '\n'; 
      }
    }
  }
  delete ct[0];
  delete ct[1];
  delete ct[2];
}
