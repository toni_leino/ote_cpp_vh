#ifndef PRJONO_H
#define PRJONO_H

#include <algorithm>
#include <cstddef>
#include <sstream>
#include <vector>

namespace otecpp_prjono
{
    class IllegalIndex: public std::exception
    {
        public:
        IllegalIndex(const std::string &msg = ""):
            msg_(std::string("Illegal index: ") + msg)
        {
        }
        ~IllegalIndex() throw()
        {
        }
        const char *what() const throw()
        {
            return msg_.c_str();
        }

        private:
        std::string msg_;
    };

    template<typename T>
    bool pienempi(const T &a, const T &b)
    {
        return a < b;
    }

    template<typename T, bool pienempi(const T&, const T&) = pienempi<T> >
    class PrJono
    {
        public:
        size_t lisaa(const T& a)
        {
            typename std::vector<T>::iterator it
                = std::lower_bound(alkiot_.begin(), alkiot_.end(), a, pienempi);
            it = alkiot_.insert(it, a);
            return it - alkiot_.begin();
        }

        size_t koko() const
        {
            return alkiot_.size();
        }

        T pienin() throw(IllegalIndex)
        {
            if (alkiot_.size() == 0) {
                throw IllegalIndex();
            }
            T tmp = alkiot_[0];
            alkiot_.erase(alkiot_.begin());
            return tmp;
        }

        T suurin() throw(IllegalIndex)
        {
            if (alkiot_.size() == 0) {
                throw IllegalIndex();
            }
            T tmp = alkiot_[alkiot_.size() - 1];
            alkiot_.erase(alkiot_.end() - 1);
            return tmp;
        }

        T poista(size_t i) throw(IllegalIndex)
        {
            if (alkiot_.size() <= i) {
                std::ostringstream ss;
                ss << i;
                throw IllegalIndex(ss.str());
            }
            T tmp = alkiot_[i];
            alkiot_.erase(alkiot_.begin() + i);
            return tmp;
        }

        const T& operator[](size_t i) const throw(IllegalIndex)
        {
            if (alkiot_.size() <= i) {
                std::ostringstream ss;
                ss << i;
                throw IllegalIndex(ss.str());
            }
            return alkiot_[i];
        }

        private:
        std::vector<T> alkiot_;
    };

    template<typename T, bool pienempi(const T&, const T&)>
    std::ostream& operator<<(std::ostream &out, const PrJono<T, pienempi> &prj)
    {
        for (size_t i = 0; i < prj.koko(); ++i) {
            out << prj[i] << ((i < prj.koko() - 1) ? " " : "");
        }
        return out;
    }
}

#endif /* end of include guard: PRJONO_H */
