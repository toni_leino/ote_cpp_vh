#include <iostream>
#include <string>
#include <vector>

#include "comparable.h"

namespace otecpp_luvut
{
  class DivideByZero: public std::exception
  {
    public:
    DivideByZero(const std::string &msg = ""):
      msg_(std::string("Divide by zero: ") + msg)
    {
    }
    ~DivideByZero() throw()
    {
    }
    const char *what() const throw()
    {
      return msg_.c_str();
    }

    private:
    std::string msg_;
  };

  class Luku
  {
    public:
      Luku();
      Luku(const Luku &b);

      virtual ~Luku();

      virtual std::string toString() const;
      virtual operator double() const;
      
      Luku& operator=(const Luku &b);
      virtual Luku plus(const Luku &b) const;

      friend Luku operator+(const Luku &a, const Luku &b);
    protected:
      Luku(Luku *luku);
    private:
      Luku *luku;
      virtual Luku *kloonaa() const;
  };

  class Integer : public Luku, public otecpp_comparable::ComparableBeta
  {
    int arvo;

    public:
    Integer(int arvo);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    int compareTo(const otecpp_comparable::ComparableBeta *b) const throw(otecpp_comparable::IllegalComparison);

    private:
    Integer *kloonaa() const;
  };

  class Double : public Luku
  {
    double arvo;

    public:
    Double(double arvo);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    private:
    Double *kloonaa() const;
  };

  class Murtoluku : public Luku, public otecpp_comparable::Comparable<Murtoluku>
  {
    int os;
    int nim;

    public:
    Murtoluku(int os, int nim) throw(DivideByZero);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    int compareTo(const Murtoluku *b) const;
    int compareTo(const otecpp_comparable::Comparable<Murtoluku> *b) const;

    private:
    Murtoluku *kloonaa() const;
    void supista();
  };

  std::ostream& operator<<(std::ostream &out, const Luku *luku);
  std::ostream& operator<<(std::ostream &out, const Luku &luku);
  std::vector<const Luku*> lajitellut(const std::vector<const Luku*> luvut);

  Luku operator+(const Luku &a, const Luku &b);
}
