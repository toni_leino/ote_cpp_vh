#include <iostream>
#include "prjono.h"

using namespace std;
using namespace otecpp_prjono;

bool isompi(const double &a, const double &b)
{
  return a > b;
}

int main()
{
  double dt[10] = {-3.2, 4.5, 2.8, 15.75, -235, -44, 0, 99.99, 12.35, 1.23};
  PrJono<double> a;
  PrJono<double, isompi> b; // Päinvastainen järjestys
  for(size_t i = 0; i < 10; ++i)
  {
    a.lisaa(dt[i]);
    b.lisaa(dt[i]);
  }
  cout << "Alustettiin jonot:\na: " << a << "\nb: " << b << '\n';
  try
  {
    cout << "a[0] ja a[10]: ";
    cout << a[0] << " ja ";
    cout << a[10] << '\n';
  }
  catch(exception &e)
  {
    cout << e.what() << '\n';
  }
  try
  {
    while(true)
    {
      size_t i = a.koko()/2;
      double d = a.poista(i);
      cout << "a[" << i << "] = " << d << " poistui: " << a << '\n';
    }
  }
  catch(exception &e)
  {
    cout << e.what() << '\n';
  }
  try
  {
    while(true)
    {
      double d = (b.koko() % 2) ? b.suurin() : b.pienin();
      cout << "b:n arvo " << d << " poistui: " << b<< '\n';
    }
  }
  catch(exception &e)
  {
    cout << e.what() << '\n';
  }
}
