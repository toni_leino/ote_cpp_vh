#include "lomitus.h"

#include <cstdlib>
#include <cstring>

using std::vector;

namespace otecpp_lomitus
{
    template<typename T>
    vector<T> lomita(const vector<T> &a, const vector<T> &b)
    {
        vector<T> tulos;
        size_t i = 0;
        size_t j = 0;

        while (i < a.size() || j < b.size()) {
            if (i >= a.size()) {
                tulos.push_back(b[j++]);
            } else if (j >= b.size()) {
                tulos.push_back(a[i++]);
            } else {
                tulos.push_back((a[i] < b[j]) ? a[i++] : b[j++]);
            }
        }
        return tulos;
    }

    template<typename T>
    vector<T*> lomita(const vector<T*> &a, const vector<T*> &b, bool kopioi)
    {
        vector<T*> tulos;
        size_t i = 0;
        size_t j = 0;

        while (i < a.size() || j < b.size()) {
            if (i >= a.size()) {
                tulos.push_back(b[j++]);
            } else if (j >= b.size()) {
                tulos.push_back(a[i++]);
            } else {
                tulos.push_back((*a[i] < *b[j]) ? a[i++] : b[j++]);
            }
        }
        if (kopioi) {
            for (i = 0; i < tulos.size(); ++i) {
                tulos[i] = new T(*tulos[i]);
            }
        }
        return tulos;
    }

    vector<const char *> lomita
            (const vector<const char *> &a, const vector<const char *> &b, bool kopioi)
    {
        vector<const char *> tulos;
        size_t i = 0;
        size_t j = 0;

        while (i < a.size() || j < b.size()) {
            if (i >= a.size()) {
                tulos.push_back(b[j++]);
            } else if (j >= b.size()) {
                tulos.push_back(a[i++]);
            } else {
                tulos.push_back((strcmp(a[i], b[j]) < 0) ? a[i++] : b[j++]);
            }
        }
        if (kopioi) {
            for (i = 0; i < tulos.size(); ++i) {
                char *tmp = static_cast<char *>(malloc(strlen(tulos[i]) + 1));
                strcpy(tmp, tulos[i]);
                tulos[i] = tmp;
            }
        }
        return tulos;
    }
}
