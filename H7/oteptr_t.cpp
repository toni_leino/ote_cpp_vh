#include <cstring>
#include <iomanip>
#include <iostream>
#include <vector>
#include "oteptr.h"

using namespace std;
using namespace otecpp_oteptr;

struct A {
  int id;
  A(int id) : id(id) {
  }
  ~A() {
    cout << "A(" << id << "):n tuhoaja\n";
  }
};

int main(int argc, char *argv[]) {
  OtePtr<char, true> mj(new char[strlen(argv[1])+1]);
  strcpy(mj.get(), argv[1]);
  OtePtr<char, true> mj2 = mj;
  cout << mj.get() << " ja " << mj2.get() << '\n';
  mj[0] = 'K';
  cout << mj.get() << " ja " << mj2.get() << '\n';
  cout << *mj << '\n'; 
  vector< OtePtr<A> > at;
  for(int i = 0; i < 4; ++i) {
    at.push_back(OtePtr<A>(new A(i)));
    cout << "Lisätty olio A(" << at.back()->id << ")\n";
  }
  OtePtr<A> a(new A(4));
  at.push_back(a);
  vector< OtePtr<A> > at2 = at;
  OtePtr<A> a2;
  try {
    a2[0] = a2[1];
  }
  catch(exception &e) {
    cout << e.what() << '\n';
  }
  try {
    cout << a2->id << '\n';
  }
  catch(exception &e) {
    cout << e.what() << '\n';
  }
  OtePtr<int, true> a3;
  try {
    cout << a3[0] << '\n';
  }
  catch(exception &e) {
    cout << e.what() << '\n';
  }
  a2 = a;
  at2.push_back(a2);
  for(int i = 0; i < 5; ++i) {
    cout << "Hallussa samat: " << boolalpha
         << (at[i].get() == at2[i].get()) << '\n';
  }
}
