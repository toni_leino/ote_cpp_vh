#include "poikkeukset.h"

#include <cstdlib>
#include <exception>
#include <iostream>
#include <sstream>

using namespace otecpp_poikkeukset;

namespace
{
    void unexpected_handler()
    {
        try {
            throw;
        } catch (int i) {
            std::ostringstream ss;
            ss << i;
            throw Unexpected(std::string("int ") + ss.str());
        } catch (...) {
            std::cout << "Terminating due to unknown unexpected exception\n";
            std::terminate();
        }
    }

    void terminator_handler()
    {
        std::cout << "Custom terminate was called\n";
        exit(EXIT_SUCCESS);
    }
}

namespace otecpp_poikkeukset
{
    void aseta_kasittelijat()
    {
        std::set_unexpected(unexpected_handler);
        std::set_terminate(terminator_handler);
    }
}
