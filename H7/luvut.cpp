#include "luvut.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <typeinfo>

using namespace otecpp_comparable;
using std::string;

namespace otecpp_luvut
{
    Luku::Luku():
        luku(NULL)
    {
    }

    Luku::Luku(const Luku &b)
    {
        if (b.luku == &b) {
            luku = b.kloonaa();
        } else if (b.luku) {
            luku = b.luku->kloonaa();
        } else {
            luku = NULL;
        }
    }

    Luku::Luku(Luku *luku):
        luku(luku)
    {
    }

    Luku::~Luku()
    {
        if (luku != this) {
            delete luku;
        }
    }

    std::string Luku::toString() const
    {
        if (luku) {
            return luku->toString();
        }
        return "Luku";
    }

    Luku::operator double() const
    {
        if (luku) {
            return static_cast<double>(*luku);
        }
        return 0;
    }

    Luku& Luku::operator=(const Luku &b)
    {
        if (luku != this) {
            delete luku;
        }
        if (b.luku == &b) {
            luku = b.kloonaa();
        } else if (b.luku) {
            luku = b.luku->kloonaa();
        } else {
            luku = NULL;
        }
        return *this;
    }

    Luku Luku::plus(const Luku &b) const
    {
        return b;
    }

    Luku *Luku::kloonaa() const
    {
        return NULL;
    }

    Integer::Integer(int arvo):
        Luku(this),
        arvo(arvo)
    {
    }

    std::string Integer::toString() const
    {
        std::ostringstream oss;
        oss << "Integer: " << arvo;
        return oss.str();
    }

    Integer::operator double() const
    {
        return arvo;
    }

    Luku Integer::plus(const Luku &b) const
    {
        const Integer *ib;
        if ((ib = dynamic_cast<const Integer*>(&b)) != NULL) {
            Integer *itmp = new Integer(arvo + ib->arvo);
            Luku ltmp(*itmp);
            delete itmp;
            return Luku(ltmp);
        } else {
            return b.plus(*this);
        }
    }

    int Integer::compareTo(const ComparableBeta *b) const throw(IllegalComparison)
    {
        const Integer *ib;
        if ((ib = dynamic_cast<const Integer*>(b)) == NULL) {
            throw IllegalComparison(string(typeid(*this).name()) + " vs. " + string(typeid(*b).name()));
            std::cout << "Laiton vertailu!\n";
            return 0;
        } else {
            if (arvo < ib->arvo) {
                return -1;
            } else if (arvo > ib->arvo) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    Integer* Integer::kloonaa() const
    {
        return new Integer(arvo);
    }

    Double::Double(double arvo):
        Luku(this),
        arvo(arvo)
    {
    }

    std::string Double::toString() const
    {
        std::ostringstream oss;
        oss << "Double: " << arvo;
        return oss.str();
    }

    Double::operator double() const
    {
        return arvo;
    }

    Luku Double::plus(const Luku &b) const
    {
        Double *dtmp = new Double(arvo + static_cast<double>(b));
        Luku ltmp(*dtmp);
        delete dtmp;
        return Luku(ltmp);
    }

    Double *Double::kloonaa() const
    {
        return new Double(arvo);
    }

    Murtoluku::Murtoluku(int os, int nim) throw(DivideByZero):
        Luku(this),
        os(os),
        nim(nim)
    {
        if (nim == 0) {
            std::ostringstream ss;
            ss << os << " / " << nim;
            throw DivideByZero(ss.str());
        } else if (nim < 0) {
            this->os *= -1;
            this->nim *= -1;
        }
        // supista();
    }

    std::string Murtoluku::toString() const
    {
        std::ostringstream ss;
        ss << "Murtoluku: ";
        ss << os << "/" << nim;
        return ss.str();
    }

    Murtoluku::operator double() const
    {
        if (os != 0 && nim != 0) {
            return static_cast<double>(os)/static_cast<double>(nim);
        } else {
            return 0;
        }
    }

    Luku Murtoluku::plus(const Luku &b) const
    {
        const Murtoluku *mb;
        if (dynamic_cast<const Double*>(&b) != NULL) {
            return b.plus(*this);
        }

        else if (dynamic_cast<const Integer*>(&b) != NULL) {
            Murtoluku *mtmp = new Murtoluku(os + (static_cast<double>(b) * nim), nim);
            Luku ltmp(*mtmp);
            delete mtmp;
            return Luku(ltmp);
        }

        else if ((mb = dynamic_cast<const Murtoluku*>(&b)) != NULL) {
            int uusi_nim = nim * mb->nim;
            int uusi_os = (os * mb->nim) + (mb->os * nim);
            Murtoluku *mtmp = new Murtoluku(uusi_os, uusi_nim);
            Luku ltmp(*mtmp);
            delete mtmp;
            return Luku(ltmp);
        }

        return Luku();
    }

    int Murtoluku::compareTo(const Murtoluku *b) const
    {
        double ad = static_cast<double>(*this);
        double bd = static_cast<double>(*b);

        if (ad < bd) {
            return -1;
        } else if (ad > bd) {
            return 1;
        } else {
            return 0;
        }
    }

    int Murtoluku::compareTo(const Comparable<Murtoluku> *b) const
    {
        return -(b->compareTo(this));
    }

    Murtoluku* Murtoluku::kloonaa() const
    {
        return new Murtoluku(os, nim);
    }

    void Murtoluku::supista()
    {
        if (os == 0 || nim == 0) {
            return;
        }
        int pieni = std::abs(os);
        int iso = std::abs(nim);
        while (pieni != iso) {
            if (iso < pieni) {
                std::swap(iso, pieni);
            }
            iso -= pieni;
        }
        os /= iso;
        nim /= iso;
    }

    std::ostream& operator<<(std::ostream &out, const Luku *luku)
    {
        out << luku->toString();
        return out;
    }

    std::ostream& operator<<(std::ostream &out, const Luku &luku)
    {
        out << luku.toString();
        return out;
    }

    namespace {
        bool luku_cmp(const Luku *a, const Luku *b)
        {
            return static_cast<double>(*a) < static_cast<double>(*b);
        }
    }

    std::vector<const Luku*> lajitellut(const std::vector<const Luku*> luvut)
    {
        std::vector<const Luku*> tmp(luvut);
        std::sort(tmp.begin(), tmp.end(), luku_cmp);
        return tmp;
    }

    Luku operator+(const Luku &a, const Luku &b)
    {
        if (!a.luku && !b.luku) {
            return Luku();
        } else if (!a.luku) {
            return b;
        } else if (!b.luku) {
            return a;
        }

        return a.luku->plus(*b.luku);
    }
}
