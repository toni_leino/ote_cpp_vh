#ifndef POIKKEUKSET_H
#define POIKKEUKSET_H

#include <exception>
#include <string>

namespace otecpp_poikkeukset
{
    class Unexpected: public std::exception
    {
        public:
        Unexpected(const std::string &msg = ""):
            msg_(std::string("Unexpected exception: ") + msg)
        {
        }
        ~Unexpected() throw()
        {
        }
        const char *what() const throw()
        {
            return msg_.c_str();
        }

        private:
        std::string msg_;
    };

    void aseta_kasittelijat();
}

#endif /* end of include guard: POIKKEUKSET_H */
