#ifndef COMPARABLE_H
#define COMPARABLE_H

#include <exception>
#include <string>

namespace otecpp_comparable
{
    class IllegalComparison: public std::exception
    {
        public:
        IllegalComparison(const std::string &msg = ""):
            msg_(std::string("Illegal comparison: ") + msg)
        {
        }
        virtual ~IllegalComparison() throw()
        {
        }
        const char *what() const throw()
        {
            return msg_.c_str();
        }

        private:
        std::string msg_;
    };

    class ComparableBeta
    {
        public:
        virtual ~ComparableBeta()
        {
        }
        virtual int compareTo(const ComparableBeta *b) const throw(IllegalComparison) = 0;
    };

    template<typename T>
    class Comparable
    {
        public:
        virtual ~Comparable()
        {
        }
        virtual int compareTo(const T *b) const = 0;
        virtual int compareTo(const Comparable<T> *b) const = 0;
    };
}

#endif /* end of include guard: COMPARABLE_H */
