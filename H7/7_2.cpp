#include <vector>

#include "lomitus.cpp"

namespace otecpp_lomitus
{
  template vector<int> lomita(const vector<int> &, const vector<int> &);
  template vector<int *> lomita(const vector<int *> &, const vector<int *> &, bool);
}
