#pragma once

#include <cstddef>
#include <string>
 
namespace otecpp_filestats
{
    struct FileStats
    {
        size_t chars;
        size_t words;
        size_t rows;
    };
 
    FileStats fileStats (const std::string &filename);
}
