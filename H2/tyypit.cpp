#include "tyypit.h"

#include <sstream>

using otecpp_tyypit::Tyypit;

Tyypit otecpp_tyypit::keraaTyypit(std::istream &virta)
{
    Tyypit tyypit;
    std::string buf;

    while (virta >> buf) {
        std::istringstream ss;
        long int tmpl;
        double tmpd;
        char tmpc;

        if (ss.str(buf), ss >> tmpl && !(ss >> tmpc)) {
            tyypit.kokonaisluvut.push_back(tmpl);
        } else if (ss.str(buf), ss >> tmpd && !(ss >> tmpc)) {
            tyypit.liukuluvut.push_back(tmpd);
        } else if (buf == "true" || buf == "false") {
            tyypit.totuusarvot.push_back(buf == "true");
        } else {
            tyypit.muut.push_back(buf);
        }
    }
    return tyypit;
}
