#pragma once

#include <string>

namespace otecpp_hirsipuu
{
    typedef unsigned int uint;
    void hirsipuu(const std::string &sanatiedosto, uint x, uint maxVirheet);
}
