#include "hirsipuu.h"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace
{
    // Lukee kaikki sanat tiedostosta ja tallentaa ne sanat-vektoriin.
    void lue_sanat(const std::string &tied, std::vector<std::string> &sanat)
    {
        std::ifstream is(tied.c_str());
        std::string sana;

        while (std::getline(is, sana)) {
            sanat.push_back(sana);
        }
    }

    void tulosta_arvattava(const std::string &arvattava,
                           const std::string &arvatut)
    {
        for (size_t i = 0; i < arvattava.size(); ++i) {
            if (arvatut.find_first_of(arvattava[i]) == std::string::npos) {
                std::cout << "_";
            } else {
                std::cout << arvattava[i];
            }
        }
    }
}

void otecpp_hirsipuu::hirsipuu
        (const std::string &sanatiedosto, uint x, uint maxVirheet)
{
    std::vector<std::string> sanat;
    lue_sanat(sanatiedosto, sanat);
    std::string arvattava_sana = sanat[x % sanat.size()];
    std::string arvatut_kirjaimet = "";
    uint virheet = 0;
    bool peli_ohi = false;

    for (;;) {
        if (!peli_ohi) {
            std::cout << "Arvattava sana on ";
            tulosta_arvattava(arvattava_sana, arvatut_kirjaimet);
            std::cout << ", vielä " << maxVirheet - virheet
                      << " virhettä jäljellä" << std::endl;
        }

        std::string buf;
        if (!std::getline(std::cin, buf)) {
            std::cout << "Virhe!" << std::endl;
            exit(1);
        }
        std::istringstream ss(buf);
        std::string komento;
        if (!(ss >> komento)) {
            std::cout << "Tuntematon komento\n";
            continue;
        } else if (komento == "uusi") {
            uint uusi_x, uusi_maxVirheet;
            if (!(ss >> uusi_x >> uusi_maxVirheet)) {
                std::cout << "Tuntematon komento\n";
            } else {
                arvattava_sana = sanat[uusi_x % sanat.size()];
                maxVirheet = uusi_maxVirheet;
                virheet = 0;
                arvatut_kirjaimet = "";
                peli_ohi = false;
            }
            continue;
        } else if (komento == "kirjain") {
            char c;
            if (!(ss >> c)) {
                std::cout << "Tuntematon komento\n";
            } else {
                if (arvattava_sana.find_first_of(c) == std::string::npos
                 || arvatut_kirjaimet.find_first_of(c) != std::string::npos) {
                    virheet++;
                    if (virheet > maxVirheet) {
                        std::cout << "Hävisit, sana oli "
                                  << arvattava_sana << "!\n";
                        peli_ohi = true;
                    }
                } else {
                    arvatut_kirjaimet += c;
                }
            }
        } else if (komento == "sana") {
            std::string arvaus;
            if (!(ss >> arvaus)) {
                std::cout << "Tuntematon komento\n";
            } else if (arvaus == arvattava_sana) {
                std::cout << "Voitit, sana oli " << arvattava_sana << "!\n";
                peli_ohi = true;
            } else {
                virheet++;
                if (virheet > maxVirheet) {
                    std::cout << "Hävisit, sana oli "
                              << arvattava_sana << "!\n";
                    peli_ohi = true;
                }
            }
        } else if (komento == "lopeta") {
            return;
        } else {
            std::cout << "Tuntematon komento\n";
        }
    }
}

#if DEBUG
#include <cstdlib>
#include <ctime>
int main ()
{
    std::srand(std::time(NULL));
    otecpp_hirsipuu::hirsipuu("words.txt", std::rand(), 6);
}
#endif // DEBUG
