#pragma once

#include <istream>
#include <ostream>

namespace otecpp_ruudukot
{
    void piirraRuudukoita(std::istream &syote, std::ostream &tuloste);
}
