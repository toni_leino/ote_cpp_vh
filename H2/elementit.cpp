#include "elementit.h"

void otecpp_elementit::sisenna
        (std::istream &syote, std::ostream &tuloste, uint sisennys)
{
    uint s = 0; // nykyinen sisennys
    char c;
    bool tagissa = false; // luetaanko juuri nyt tagia?
    bool tyhja_rivi = true; // onko nykyiselle riville vielä tulostettu merkkejä?
    bool sisennetty = false; // onko tämä rivi jo sisennetty?
    std::string tagi;
    std::string buf;

    while (syote.get(c)) {
        if (tagissa) {
            tagi += c;
            if (c == '>') {
                tagissa = false;
                if (tagi[1] == '/') { // lopputagi?
                    s--;
                    if (!tyhja_rivi) {
                        tuloste << "\n";
                        sisennetty = false;
                    }
                    if (!sisennetty) {
                        tuloste << std::string(sisennys*s, ' ');
                    }
                    tuloste << tagi << "\n";
                } else if (tagi[tagi.size()-2] == '/') {// yksittäinen tagi?
                    tuloste << " " << tagi << " ";
                    tyhja_rivi = true;
                    tagi = "";
                    continue;
                } else { // alkutagi
                    if (!tyhja_rivi) {
                        tuloste << "\n";
                        sisennetty = false;
                    }
                    if (!sisennetty) {
                        tuloste << std::string(sisennys*s, ' ');
                    }
                    tuloste << tagi << "\n";
                    s++;
                }
                tagi = "";
                tyhja_rivi = true;
                sisennetty = false;
            }
        } else if (c == '<') {
            tagissa = true;
            tagi += c;
            tuloste << buf.substr(0, buf.find_last_not_of(" ")+1);
            buf = "";
        } else if (c != '\n') {
            if (c != ' ' || !tyhja_rivi) {
                if (!sisennetty) {
                    tuloste << std::string(sisennys*s, ' ');
                }
                sisennetty = true;
                buf += c;
                tyhja_rivi = false;
            }
        }
    }
}

#if DEBUG
#include <fstream>
int main(int argc, char *argv[])
{
    if (argc < 3) {
        goto end;
    }

    {
        std::ifstream syote(argv[1]);
        std::ofstream tuloste(argv[2]);
        otecpp_elementit::sisenna(syote, tuloste, 2);
    }
end:
    ;
}
#endif
