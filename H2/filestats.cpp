// Fun fact: kirjoitin tämän koodin itse tehdyllä tekstieditorilla, jonka nimi on Loony.
// Editorissa oli 283 riviä koodia (C:tä tietysti) kun kirjoitin tämän.
 
#include "filestats.h"
#include <fstream> // tämä riittänee
#include <sstream>
 
otecpp_filestats::FileStats otecpp_filestats::fileStats
        (const std::string &filename)
{
    std::ifstream is(filename.c_str());
    otecpp_filestats::FileStats stats = {0};
    std::string buf;
 
    while (std::getline(is, buf)) {
        stats.chars += buf.length();
        if (!is.eof())
            stats.chars += 1; // rivinvaihto
        stats.rows += 1;
        std::istringstream ss(buf);
        std::string tmp;
        while (ss >> tmp)
            stats.words += 1;
    }
 
    return stats;
}
