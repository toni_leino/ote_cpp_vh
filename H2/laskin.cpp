#include "laskin.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace
{
    bool on_operaattori (const std::string &s)
    {
        return s == "+" || s == "-" || s == "*" || s == "/";
    }

    // Palauttaa true jos lasku onnistui
    bool suorita_lasku (std::vector<double> &pino, const std::string &op)
    {
        if (pino.size() < 2 || !on_operaattori(op)
            || (op == "/" && pino.back() == 0)) {
            return false;
        }

        double y = pino.back();
        pino.pop_back();
        double x = pino.back();
        pino.pop_back();
        pino.push_back(op == "+" ? x + y :
                       op == "-" ? x - y :
                       op == "*" ? x * y :
                                   x / y);
        return true;
    }
}

void otecpp_laskin::laskin()
{
    std::string buf;
    while (std::getline(std::cin, buf)) {
        if (buf == "lopeta") {
            return;
        }

        std::vector<double> pino;
        std::istringstream ss(buf);
        std::string tmp;
        while (ss >> tmp) {
            std::istringstream muuntaja(tmp); // muuntaa merkkijonoja luvuiksi
            double luku;
            if (on_operaattori(tmp)) {
                if (!suorita_lasku(pino, tmp)) {
                    std::cout << "Virheellinen laskukaava!\n";
                    goto seuraava_rivi;
                }
            } else if (muuntaja >> luku) {
                pino.push_back(luku);
            } else {
                std::cout << "Virheellinen laskukaava!\n";
                goto seuraava_rivi;
            }
        }
        if (pino.size() != 1) {
            std::cout << "Virheellinen laskukaava!\n";
            goto seuraava_rivi;
        } else {
            std::cout << pino.back() << "\n";
        }

seuraava_rivi:
        ;
    }
}
