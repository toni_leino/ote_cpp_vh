#include <iostream>
#include <string>
#include "filestats.h"

using namespace std;

using otecpp_filestats::FileStats;
using otecpp_filestats::fileStats;

int main(int argc, char *argv[])
{
  FileStats fs = fileStats(string(argv[1]));
  cout << "Merkkejä: " << fs.chars << endl;
  cout << "Sanoja: " << fs.words << endl;
  cout << "Rivejä: " << fs.rows << endl;
}
