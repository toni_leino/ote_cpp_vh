#pragma once

#include <iostream>

namespace otecpp_elementit
{
    typedef unsigned int uint;
    void sisenna(std::istream &syote, std::ostream &tuloste, uint sisennys);
}
