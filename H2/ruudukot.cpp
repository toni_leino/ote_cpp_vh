#include "ruudukot.h"

#include <sstream>
#include <string>

void otecpp_ruudukot::piirraRuudukoita
        (std::istream &syote, std::ostream &tuloste)
{
    std::string buf;

    while (std::getline(syote, buf)) {
        std::string komento;
        int x = 1, y = 1, z;

        std::istringstream ss(buf);
        if (!(ss >> komento)) {
            tuloste << "Tuntematon komento!\n";
            continue;
        } else if (komento == "lopeta") {
            return;
        } else if (komento == "ruudukko") {
            goto ruudukko;
        } else {
            tuloste << "Tuntematon komento!\n";
            continue;
        }

ruudukko:
        if (!(ss >> x >> y)) {
            tuloste << "Tuntematon komento!\n";
            continue;
        }
        if (!(ss >> z)) {
            z = 1;
        }
        if (x < 1 || y < 1 || z < 1) {
            tuloste << "Tuntematon komento!\n";
            continue;
        }

        for (int riv = 0; riv <= (x*z) + x; ++riv) {
            if (!(riv % (z+1))) {
                tuloste << std::string((z*y)+y+1, '*') << "\n";
                continue;
            }
            for (int sar = 0; sar <= (y*z) + y; ++sar) {
                tuloste << (!(sar % (z+1)) ? '*' : ' ');
            }
            tuloste << "\n";
        }
    }
}
