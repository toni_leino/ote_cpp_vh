#include "tyypit.h"

#include <fstream>
#include <iostream>

using namespace otecpp_tyypit;

int main ()
{
    std::ifstream is("koe.txt");
    Tyypit tyypit = keraaTyypit(is);

    for (int i = 0; i < tyypit.kokonaisluvut.size(); ++i) {
        std::cout << tyypit.kokonaisluvut[i] << " ";
    }
    std::cout << "\n";
    for (int i = 0; i < tyypit.liukuluvut.size(); ++i) {
        std::cout << tyypit.liukuluvut[i] << " ";
    }
    std::cout << "\n";
    for (int i = 0; i < tyypit.totuusarvot.size(); ++i) {
        std::cout << tyypit.totuusarvot[i] << " ";
    }
    std::cout << "\n";
    for (int i = 0; i < tyypit.muut.size(); ++i) {
        std::cout << tyypit.muut[i] << " ";
    }
    std::cout << "\n";
    
}
