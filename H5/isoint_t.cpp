#include <iostream>
#include <sstream>

#include "isoint.h"

using namespace std;
using namespace otecpp_isounsigned;
using namespace otecpp_isoint;

int main()
{
    IsoInt a(128);
    IsoInt b(64);
    cout << "a: " << a << " ja b: " << b << '\n';
    for(int i = 1; i <= 5; ++i)
    {
        cout << "a = a + (a - b): " << a + (a - b) << '\n';
        a += (a - b);
        cout << "b = b - (b + a): " << b - (b + a) << '\n';
        b -= (b + a); // Loppujen lopuksi sama kuin b = -a.
    }
    for(int i = 1; i <= 50; ++i)
    {  // Tehdään luvuista hieman isompia/pienempiä...
        a += (a - b);
        b -= (b + a);
    }
    const IsoUnsigned &y = b; // Testataan polymorfisuutta.
    ostringstream oss; // Testataan tulostus...
    oss << "a: " << a << "ja b: " << y;  // y viittaa oikeasti IsoInt-olioon
#if DEBUG
    cout << "-----\n";
    cout << oss.str() << endl;
    cout << "-----\n";
#endif
    IsoInt a2;
    IsoInt b2;
    string s1;
    string s2;
    string s3;
    istringstream iss(oss.str());  // ...ja luku.
    if(iss >> s1 >> a2 >> s2 >> s3 >> b2)
    {
        cout << s1 << ' ' << a2 << ' ' << s2 << ' ' << s3 << ' ' << b2 << '\n';
    }
    IsoUnsigned a3 = a;  // Käsitellään IsoUnsigned-oliona
    IsoUnsigned b3 = b;  // eli tässä etumerkki jää pois.
    cout << "a: " << a3 << " ja b3: " << b3 << '\n';
}
