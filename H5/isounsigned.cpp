#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

#include "isounsigned.h"

using namespace std;

namespace otecpp_isounsigned
{
    IsoUnsigned& IsoUnsigned::operator++()
    {
        for (size_t n = luku.size(); n-- > 0;) {
            if (++(luku[n]) <= '9')
                goto ret;
            else {
                luku[n] = '0';
            }
        }

        luku.insert(0, 1, '1');
ret:
        return *this;
    }

    IsoUnsigned IsoUnsigned::operator++(int)
    {
        IsoUnsigned tmp = *this;
        operator++();
        return tmp;
    }

    IsoUnsigned& IsoUnsigned::operator+=(const IsoUnsigned &b)
    {
        IsoUnsigned tmp = *this + b;
        luku = tmp.luku;
        return *this;
    }

    IsoUnsigned& IsoUnsigned::operator-=(const IsoUnsigned &b)
    {
        IsoUnsigned tmp = *this - b;
        luku = tmp.luku;
        return *this;
    }

    bool IsoUnsigned::operator<(const IsoUnsigned &b) const
    {
        if (luku.size() != b.luku.size()) {
            return luku.size() < b.luku.size();
        } else {
            return luku < b.luku;
        }
    }

    bool IsoUnsigned::operator>(const IsoUnsigned &b) const
    {
        return b < *this;
    }

    bool IsoUnsigned::operator==(const IsoUnsigned &b) const
    {
        return luku == b.luku;
    }

    bool IsoUnsigned::operator!=(const IsoUnsigned &b) const
    {
        return !(*this == b);
    }

    IsoUnsigned operator+(const IsoUnsigned &eka, const IsoUnsigned &toinen)
    {
        IsoUnsigned tulos;
        tulos.luku = "";
        string::size_type i = eka.luku.size();
        string::size_type j = toinen.luku.size();
        int arvo = 0;
        while((i > 0) || (j > 0))
        {
            char a = (i > 0) ? eka.luku[--i] : '0';
            char b = (j > 0) ? toinen.luku[--j] : '0';
            arvo = arvo + (a -'0') + (b -'0');
            tulos.luku.insert(0, 1, (arvo % 10) + '0');
            arvo = arvo/10;
        }
        if(arvo > 0)
        {
            tulos.luku.insert(0, 1, (arvo % 10) + '0');
        }
        return tulos;
    }

    IsoUnsigned operator-(const IsoUnsigned &eka, const IsoUnsigned &toinen)
    {
        IsoUnsigned tulos;
        std::string tulos_str(eka.luku);

        std::string eka_tmp(eka.luku);
        std::string toka_tmp(toinen.luku);
        toka_tmp.insert(0, eka_tmp.size() - toka_tmp.size(), '0');
        for (size_t i = eka.luku.size(); i-- > 0;) {
            if (eka_tmp[i] < toka_tmp[i]) {
                eka_tmp[i] += 10;
                eka_tmp[i-1] -= 1;
            }
            tulos_str[i] = '0' + (eka_tmp[i] - toka_tmp[i]);
        }
        while (tulos_str[0] == '0') {
            tulos_str.erase(0, 1);
        }
        tulos.luku = tulos_str;
        return tulos;
    }

    void IsoUnsigned::tulosta(ostream &out) const
    {
        out << luku;
    }

    void IsoUnsigned::lue(istream &in)
    {
        while (isspace(in.peek())) {
            in.get();
        }

        luku = "";
        if (!isdigit(in.peek())) {
            in.setstate(ios_base::failbit);
            return;
        }

        char c;
        while (isdigit(c = in.peek())) {
            luku += c;
            in.get();
        }
    }

    ostream & operator<<(ostream &out, const IsoUnsigned &arvo)
    {
        arvo.tulosta(out);
        return out;
    }
    
    istream & operator>>(istream &in, IsoUnsigned &arvo)
    {
        arvo.lue(in);
        return in;
    }
}
