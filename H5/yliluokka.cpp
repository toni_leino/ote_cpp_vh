#include "yliluokka.h"

#include <cstring> // strcmp
#include <iostream>
#include <typeinfo>

using namespace otecpp_yliluokka;

namespace
{
    // Kahden olion välinen periytymissuhde
    enum Relation
    {
        REL_SAME,       // oliot edustavat samaa luokkaa
        REL_SUPERCLASS, // ensimmäinen olio on toisen yliluokka
        REL_SUBCLASS,   // ensimmäinen olio on toisen aliluokka
        REL_UNRELATED   // oliot eivät ole toistensa yli- tai aliluokkia
    };

    // Tutkii olioiden a ja b välisen suhteen
    Relation get_relation(const Yliluokka *a, const Yliluokka *b)
    {
        bool a_superclass_of_b = a->onYhteensopiva(b);
        bool b_superclass_of_a = b->onYhteensopiva(a);

        if (a_superclass_of_b && b_superclass_of_a) {
            return REL_SAME;
        } else if (a_superclass_of_b && !b_superclass_of_a) {
            return REL_SUPERCLASS;
        } else if (!a_superclass_of_b && b_superclass_of_a) {
            return REL_SUBCLASS;
        } else {
            return REL_UNRELATED;
        }
    }

    // Solmu puussa, joka esittää luokkahierarkiaa.
    struct ClassHierarchyNode
    {
        // Solmun tyyppi
        const Yliluokka *obj_;
        // Solmun tyypin aliluokat
        std::vector<ClassHierarchyNode*> subclasses_;

        // Rakentaja
        ClassHierarchyNode(const Yliluokka *obj):
            obj_(obj)
        {
        }
        
        // Tuhoaja
        ~ClassHierarchyNode()
        {
            for (size_t i = 0; i < subclasses_.size(); ++i) {
                delete subclasses_[i];
            }
        }

        // Lisää olion puuhun
        void insert_obj(const Yliluokka *obj)
        {
            Relation rel = get_relation(obj_, obj);
            if (rel != REL_SUPERCLASS) {
                return;
            }
            ClassHierarchyNode *tmp = new ClassHierarchyNode(obj);
            for (std::vector<ClassHierarchyNode*>::iterator it = subclasses_.begin();
                    it != subclasses_.end();) {
                if (get_relation(obj, (*it)->obj_) == REL_SAME) {
                    // luokka on jo puussa
                    delete tmp;
                    return;
                } else if (get_relation(obj, (*it)->obj_) == REL_SUPERCLASS) {
                    // uusi luokka täytyy lisätä edellisten väliin
                    tmp->insert_subclass(*it);
                    it = subclasses_.erase(it);
                } else if (get_relation(obj, (*it)->obj_) == REL_SUBCLASS) {
                    // uusi luokka tulee puun lehdeksi
                    (*it)->insert_obj(obj);
                    delete tmp;
                    return;
                } else {
                    ++it;
                }
            }
            insert_subclass(tmp);
        }

        // Lisää solmun aliluokkataulukkoon
        void insert_subclass(ClassHierarchyNode *node)
        {
            for (size_t i = 0; i < subclasses_.size(); ++i) {
                if (strcmp(typeid(*(node->obj_)).name(), typeid(*(subclasses_[i]->obj_)).name()) < 0) {
                    subclasses_.insert(subclasses_.begin() + i, node);
                    return;
                }
            }
            subclasses_.push_back(node);
        }

        // Tulostaa puun
        void print(std::ostream &os, int indent = 0)
        {
            os << std::string(indent, ' ') << typeid(*obj_).name() << '\n';
            for (size_t i = 0; i < subclasses_.size(); ++i) {
                subclasses_[i]->print(os, indent + 2);
            }
        }
    };
}

void otecpp_yliluokka::luokat(std::vector<const Yliluokka*> oliot)
{
    Yliluokka *tmp = new Yliluokka;
    ClassHierarchyNode root(tmp);
    for (size_t i = 0; i < oliot.size(); ++i) {
        root.insert_obj(oliot[i]);
    }
    root.print(std::cout);
    delete tmp;
}
