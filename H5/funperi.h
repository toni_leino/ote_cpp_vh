#ifndef FUNPERI_H
#define FUNPERI_H

#include <iostream>
#include <string>

namespace otecpp_funperi
{
    struct A
    {
        std::string kuvaus_;

        A(const std::string &nimi):
            kuvaus_(std::string("A-olio ") + nimi)
        {
        }

        virtual std::string lueKuvaus() const
        {
            return kuvaus_;
        }

        virtual void kuka(const A &a) const
        {
            std::cout << "Olen " << kuvaus_ << " ja parametrini on "
                      << a.kuvaus_ << "  [tai " << a.lueKuvaus() << "]\n";
                        
        }
    };

    struct B: public A
    {
        std::string kuvaus_;

        B(const std::string &nimi):
            A(nimi + "(B:n osana)"),
            kuvaus_(std::string("B-olio ") + nimi)
        {
        }

        std::string lueKuvaus() const
        {
            return kuvaus_;
        }

        void kuka(const A &a) const
        {
            A::kuka(a);
        }

        void kuka(const B &b) const
        {
            std::cout << "Olen " << kuvaus_ << " ja parametrini on "
                      << b.kuvaus_ << "  [tai " << b.lueKuvaus() << "]\n";
                        
        }

    };
}

#endif /* end of include guard: FUNPERI_H */
