#include "tyypit.h"

#include <iostream>

#include "mersenne.h"

using namespace otecpp_luvut;

namespace
{
    MTRand r(2014);
}

namespace otecpp_tyypit
{
    std::vector<Luku*> arvoLukuja(std::vector<Luku*>::size_type koko)
    {
        std::vector<Luku*> v;
        while (koko--) {
            switch (r.randInt() % 3) {
                case 0:
                    v.push_back(new Integer(r.randInt() % 100));
                    break;
                case 1:
                    v.push_back(new Double((r.randInt() % 100) + static_cast<double>(r.randInt() % 100)/100));
                    break;
                case 2:
                    {
                        // Luvut täytyy arpoa ennen funktiokutsua, sillä standardi
                        // ei määrittele, missä järjestyksessä funktion argumentit
                        // lasketaan.
                        int os = r.randInt() % 100;
                        int nim = (r.randInt() % 99) + 1;
                        v.push_back(new Murtoluku(os, nim));
                    }
                    break;
            }
        }

        return v;
    }

    void laskeLuvut(const std::vector<Luku*> &luvut)
    {
        int integerit = 0;
        int doublet = 0;
        int murtoluvut = 0;
        for (size_t i = 0; i < luvut.size(); ++i) {
            std::string tmp = luvut[i]->toString();
            // lol
            switch (tmp[0]) {
                case 'I':
                    integerit++;
                    break;
                case 'D':
                    doublet++;
                    break;
                case 'M':
                    murtoluvut++;
                    break;
                default:
                    break;
            }
        }

        std::cout << "Integer: " << integerit << " kpl\n";
        std::cout << "Double: " << doublet << " kpl\n";
        std::cout << "Murtoluku: " << murtoluvut << " kpl\n";
    }
}
