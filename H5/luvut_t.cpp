#include <iostream>
#include <vector>
#include "luvut.h"

using namespace std;
using namespace otecpp_luvut;

int main()
{
    Murtoluku ml(-3, 5);
    Double d(9.85);
    Integer i(-321);
    cout << ml << " + " << d << " + " << i << " = " << ml + d + i << '\n';
    Luku *lt[3] = {&ml, &d, &i};
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            cout << lt[i] << " + " << lt[j] << " = " << *lt[i] + *lt[j] << '\n';
        }
    }
}
