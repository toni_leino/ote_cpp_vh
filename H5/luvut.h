#include <iostream>
#include <string>
#include <vector>

namespace otecpp_luvut
{
  class Luku
  {
    public:
      Luku();
      Luku(const Luku &b);

      virtual ~Luku();

      virtual std::string toString() const;
      virtual operator double() const;
      
      Luku& operator=(const Luku &b);
      virtual Luku plus(const Luku &b) const;

      friend Luku operator+(const Luku &a, const Luku &b);
    protected:
      Luku(Luku *luku);
    private:
      Luku *luku;
      virtual Luku *kloonaa() const;
  };

  class Integer : public Luku
  {
    int arvo;

    public:
    Integer(int arvo);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    private:
    Integer *kloonaa() const;
  };

  class Double : public Luku
  {
    double arvo;

    public:
    Double(double arvo);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    private:
    Double *kloonaa() const;
  };

  class Murtoluku : public Luku
  {
    int os;
    int nim;

    public:
    Murtoluku(int os, int nim);

    std::string toString() const;
    operator double() const;
    Luku plus(const Luku &b) const;

    private:
    Murtoluku *kloonaa() const;
    void supista();
  };

  std::ostream& operator<<(std::ostream &out, const Luku *luku);
  std::ostream& operator<<(std::ostream &out, const Luku &luku);
  std::vector<const Luku*> lajitellut(const std::vector<const Luku*> luvut);

  Luku operator+(const Luku &a, const Luku &b);
}
