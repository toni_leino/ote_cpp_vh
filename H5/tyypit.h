#ifndef TYYPIT_H
#define TYYPIT_H

#include "luvut.h"

namespace otecpp_tyypit
{
    std::vector<otecpp_luvut::Luku*> arvoLukuja
            (std::vector<otecpp_luvut::Luku*>::size_type koko);
    void laskeLuvut(const std::vector<otecpp_luvut::Luku*> &luvut);
}

#endif /* end of include guard: TYYPIT_H */
