#include "funperi.h"

using otecpp_funperi::A;
using otecpp_funperi::B;

int main()
{
  A a("a");
  B b("b");
  a.kuka(a);
  a.kuka(b);
  b.kuka(a);
  b.kuka(b);
}
