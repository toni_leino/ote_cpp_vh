#ifndef ISOINT_H
#define ISOINT_H

#include "isounsigned.h"

namespace otecpp_isoint
{
    class IsoInt: public otecpp_isounsigned::IsoUnsigned
    {
        public:
            IsoInt(long int arvo = 0);
            
            IsoInt& operator+=(const IsoInt &b);
            IsoInt& operator-=(const IsoInt &b);
            
            friend IsoInt operator+(const IsoInt &a, const IsoInt &b);
            friend IsoInt operator-(const IsoInt &a, const IsoInt &b);

            void tulosta(std::ostream &out) const;
            void lue(std::istream &in);

        private:
            bool neg_; // true, jos luku on negatiivinen
    };
}

#endif /* end of include guard: ISOINT_H */
