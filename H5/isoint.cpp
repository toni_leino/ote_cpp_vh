#include "isoint.h"

#include <cctype>
#include <cmath>
#include <algorithm>

using namespace otecpp_isounsigned;

namespace otecpp_isoint {
    IsoInt::IsoInt(long int arvo):
        IsoUnsigned(std::abs(arvo)),
        neg_(arvo < 0)
    {
    }

    IsoInt& IsoInt::operator+=(const IsoInt &b)
    {
        IsoInt tmp = *this + b;
        *this = tmp;
        return *this;
    }

    IsoInt& IsoInt::operator-=(const IsoInt &b)
    {
        IsoInt tmp = *this - b;
        *this = tmp;
        return *this;
    }

    IsoInt operator+(const IsoInt &a, const IsoInt &b)
    {
        IsoInt tmp = std::max(a, b);
        if (a.neg_ == b.neg_) {
            tmp.IsoUnsigned::operator+=(std::min(a, b));
        } else {
            tmp.IsoUnsigned::operator-=(std::min(a, b));
        }
        if (tmp.nolla()) {
            tmp.neg_ = false;
        } else if (a.neg_ == b.neg_) {
            tmp.neg_ = a.neg_;
        } else if (a > b && !a.neg_) {
            tmp.neg_ = false;
        } else if (a > b && a.neg_) {
            tmp.neg_ = true;
        } else if (a < b && !b.neg_) {
            tmp.neg_ = false;
        } else {
            tmp.neg_ = true;
        }
        return tmp;
    }

    IsoInt operator-(const IsoInt &a, const IsoInt &b)
    {
        // a - b == a + (-b)
        IsoInt tmp = b;
        tmp.neg_ = !tmp.neg_;
        return a + tmp;
    }

    void IsoInt::tulosta(std::ostream &out) const
    {
        if (neg_) {
            out << '-';
        }
        IsoUnsigned::tulosta(out);
    }

    void IsoInt::lue(std::istream &in)
    {
        while (isspace(in.peek())) {
            in.get();
        }
        if (in.peek() == '-') {
            neg_ = true;
            in.get(); // miinusmerkki pois virrasta
        } else {
            neg_ = false;
        }
        IsoUnsigned::lue(in);
    }
}
