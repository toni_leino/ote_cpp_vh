#include <cstdlib>
#include <typeinfo>
#include <vector>
#include "yliluokka.h"

using namespace std;
using namespace otecpp_yliluokka;

class A : public Yliluokka
{
  public:
  bool onYhteensopiva(const Yliluokka *olio) const
  {
    return (dynamic_cast<const A*>(olio) != NULL);
  }
};
class B : public Yliluokka
{
  public:
  bool onYhteensopiva(const Yliluokka *olio) const
  {
    return (dynamic_cast<const B*>(olio) != NULL);
  }
};
class C : public A
{
  public:
  bool onYhteensopiva(const Yliluokka *olio) const
  {
    return (dynamic_cast<const C*>(olio) != NULL);
  }
};
class D : public B
{
  public:
  bool onYhteensopiva(const Yliluokka *olio) const
  {
    return (dynamic_cast<const D*>(olio) != NULL);
  }
};
class E : public B
{
  public:
  bool onYhteensopiva(const Yliluokka *olio) const
  {
    return (dynamic_cast<const E*>(olio) != NULL);
  }
};
int main()
{
  vector<const Yliluokka *> oliot;
  oliot.push_back(new B);
  oliot.push_back(new C);
  oliot.push_back(new D);
  oliot.push_back(new E);
  oliot.push_back(new B);
  oliot.push_back(new C);
  oliot.push_back(new D);
  oliot.push_back(new E);
  luokat(oliot);
  for(vector<const Yliluokka *>::size_type i = 0; i < oliot.size(); ++i)
  {
    delete oliot[i];
  }
}
