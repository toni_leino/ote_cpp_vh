#ifndef YLILUOKKA_H
#define YLILUOKKA_H

#include <vector>

namespace otecpp_yliluokka
{
    class Yliluokka
    {
        public:
            virtual ~Yliluokka()
            {
            }

            virtual bool onYhteensopiva(const Yliluokka *olio) const
            {
                return true;
            }
    };

    void luokat(std::vector<const Yliluokka*> oliot);
}

#endif /* end of include guard: YLILUOKKA_H */
