#include "pakkaus.h"

namespace
{
    // Etsii pisimmän osamerkkijonon, jonka pituus >= 2 ja
    // joka esiintyy merkkijonossa ainakin kahdesti.
    // Jos sellaista ei löydy, palautetaan tyhjä merkkijono.
    std::string pisin_osamjono (const std::string &teksti)
    {
        size_t pit = teksti.length(); // koko merkkijonon pituus

        for (size_t opit = pit / 2; opit >= 2; --opit) {
            bool loytynyt = false; // onko jo löydetty kahdesti esiintyvä osamerkkijono?
            std::string paras_mj; // tähän mennessä paras löytynyt osamerkkijono
            size_t paras_lkm = 0; // montako kertaa se esiintyy?
            for (size_t pos = 0; pos < pit - opit; ++pos) {
                std::string etsittava_mj = teksti.substr(pos, opit);
                size_t lkm = 1; // montako kertaa etsittävä merkkijono esiintyy?
                size_t pos2 = pos + opit;
                while ((pos2 = teksti.find(etsittava_mj, pos2)) != std::string::npos) {
                    lkm++;
                    pos2 = pos2 + opit;
                }
                if (lkm >= 2 &&
                    (!loytynyt || lkm > paras_lkm
                     || (lkm == paras_lkm && etsittava_mj < paras_mj))) {
                    paras_mj = etsittava_mj;
                    paras_lkm = lkm;
                    loytynyt = true;
                }
            }
            if (loytynyt) {
                return paras_mj;
            }
        }
        return std::string("");
    }

    // Korvaa tekstin kaikki mita-merkkijonot milla-merkkijonolla.
    void korvaa_kaikki (std::string &teksti,
                        const std::string &mita, const std::string &milla)
    {
        size_t pos;
        while ((pos = teksti.find(mita)) != std::string::npos) {
            teksti.replace(pos, mita.length(), milla, 0, milla.length());
        }
    }
}

std::vector<std::string> otecpp_pakkaus::pakkaa (std::string &teksti)
{
    std::string kmerkki("A");
    std::vector<std::string> korvaukset;

    std::string pisin;
    while ((pisin = pisin_osamjono(teksti)) != "" && kmerkki[0] <= 'Z') {
        korvaukset.push_back(pisin);
        korvaa_kaikki(teksti, pisin, kmerkki);
        kmerkki[0]++;
    }

    return korvaukset;
}

void otecpp_pakkaus::pura
        (std::string &teksti, const std::vector<std::string> &korvaukset)
{
    std::string kmerkki(" ");
    kmerkki[0] = 'A' + korvaukset.size() - 1;
    for (size_t i = korvaukset.size(); i > 0; --i) {
        korvaa_kaikki(teksti, kmerkki, korvaukset[i-1]);
        kmerkki[0]--;
    }
}
