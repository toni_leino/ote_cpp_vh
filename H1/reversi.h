#pragma once

#include <vector>
#include <string>

// vähemmän kirjoittamista
typedef std::vector<std::string> Lauta;

namespace otecpp_reversi
{
    std::vector<std::string> siirrot (Lauta lauta, bool onValkea);
    
    bool siirto (Lauta &lauta, std::string ruutu, bool onValkea);
}
