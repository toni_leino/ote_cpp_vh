#include "reversi.h"

#include <algorithm>

namespace
{
    const char VALKEA = 'V';
    const char MUSTA  = 'M';
    const char TYHJA  = ' ';

    struct Ruutu
    {
        int riv;
        int sar;
    };

    typedef std::vector<Ruutu> Ketju;

    // Tarkistaa onko ruutu laudalla.
    bool on_laudalla (Ruutu ruutu)
    {
        return ruutu.riv >= 0 && ruutu.riv < 8
            && ruutu.sar >= 0 && ruutu.sar < 8;
    }

    // Muuttaa laudan indeksit vastaavaksi ruuduksi.
    std::string ruudun_nimi (int riv, int sar)
    {
        std::string tmp("sr");

        Ruutu ruutu = {riv, sar};
        if (!on_laudalla(ruutu)) {
            return tmp;
        }
        
        tmp[0] = 'a' + sar;
        tmp[1] = '1' + riv;
        return tmp;
    }

    // Edellinen päinvastoin: muuttaa merkkijonon indekseiksi.
    Ruutu ruudun_indeksit (const std::string &mj)
    {
        Ruutu ruutu;
        ruutu.sar = mj[0] - 'a';
        ruutu.riv = mj[1] - '1';
        return ruutu;
    }

    // Laskee ketjun joka kulkee annettuun suuntaan.
    Ketju laske_ketju (Lauta &lauta, Ruutu ruutu, int dx, int dy, bool onValkea)
    {
        Ketju ketju;

        const char ketjun_vari = onValkea ? MUSTA : VALKEA;
        
        ruutu.sar += dx;
        ruutu.riv += dy;

        for (;;) {
            if (!on_laudalla(ruutu) || lauta[ruutu.riv][ruutu.sar] == TYHJA) {
                // ketju katkesi, palautetaan tyhjä ketju
                return Ketju();
            } else if (lauta[ruutu.riv][ruutu.sar] == ketjun_vari) {
                // ketju jatkuu vielä
                ketju.push_back(ruutu);
            } else {
                // ketju päättyi
                return ketju;
            }
            ruutu.sar += dx;
            ruutu.riv += dy;
        }
    }

    // Etsii kaikki ketjut, jotka alkavat annetusta ruudusta.
    std::vector<Ketju> laske_ketjut (Lauta &lauta, Ruutu ruutu, bool onValkea)
    {
        std::vector<Ketju> ketjut;

        for (int dx = -1; dx <= 1; ++dx) {
            for (int dy = -1; dy <= 1; ++dy) {
                if (dx == 0 && dy == 0) {
                    continue;
                }
                ketjut.push_back(laske_ketju(lauta, ruutu, dx, dy, onValkea));

                // poistetaan tyhjät ketjut
                if (ketjut.back().size() == 0) {
                    ketjut.pop_back();
                }
            }
        }

        return ketjut;
    }

    // Laskee, montako nappulaa laudalla jo on.
    unsigned int laske_nappulat (Lauta &lauta)
    {
        unsigned int nappulat = 0;

        for (unsigned int riv = 0; riv < lauta.size(); ++riv) {
            for (unsigned int sar = 0; sar < lauta[0].size(); ++sar) {
                if (lauta[riv][sar] != TYHJA) {
                    nappulat++;
                }
            }
        }

        return nappulat;
    }

    // Palauttaa neljästä keskimmäisestä ruudusta ne, jotka ovat tyhjiä.
    std::vector<std::string> tyhjat_keskiruudut (Lauta &lauta)
    {
        static const Ruutu RUUDUT[] = {
            {3, 3}, {3, 4}, {4, 3}, {4, 4}
        };

        std::vector<std::string> tyhjat;
        
        for (unsigned int i = 0; i < 4; ++i) {
            Ruutu r = RUUDUT[i];
            if (lauta[r.riv][r.sar] == TYHJA) {
                tyhjat.push_back(ruudun_nimi(r.riv, r.sar));
            }
        }

        return tyhjat;
    }
}

std::vector<std::string> otecpp_reversi::siirrot (Lauta lauta, bool onValkea)
{
    if (laske_nappulat(lauta) < 4) {
        std::vector<std::string> mahd_siirrot = tyhjat_keskiruudut(lauta);
        std::sort(mahd_siirrot.begin(), mahd_siirrot.end());
        return mahd_siirrot;
    }

    std::vector<std::string> mahd_siirrot;

    for (size_t riv = 0; riv < lauta.size(); ++riv) {
        for (size_t sar = 0; sar < lauta[0].size(); ++sar) {
            if (lauta[riv][sar] != TYHJA) {
                continue;
            }

            Ruutu ruutu = {riv, sar};
            std::vector<Ketju> ketjut =
                    laske_ketjut(lauta, ruutu, onValkea);
            if (ketjut.size() > 0) {
                mahd_siirrot.push_back(ruudun_nimi(riv, sar));
            }
        }
    }

    std::sort(mahd_siirrot.begin(), mahd_siirrot.end());
    return mahd_siirrot;
}

bool otecpp_reversi::siirto (Lauta &lauta, std::string ruutu_mj, bool onValkea)
{
    Ruutu ruutu = ruudun_indeksit(ruutu_mj);
    if (lauta[ruutu.riv][ruutu.sar] != TYHJA) {
        return false;
    }

    std::vector<Ketju> ketjut = laske_ketjut(lauta, ruutu, onValkea);
    if (ketjut.size() == 0) {
        return false;
    }

    lauta[ruutu.riv][ruutu.sar] = onValkea ? VALKEA : MUSTA;
    for (size_t i = 0; i < ketjut.size(); ++i) {
        for (size_t r = 0; r < ketjut[i].size(); ++r) {
            lauta[ketjut[i][r].riv][ketjut[i][r].sar] =
                    onValkea ? VALKEA : MUSTA;
        }
    }
    return true;
}
