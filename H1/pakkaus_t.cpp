#include <iostream>
#include <string>
#include <vector>

#include "pakkaus.h"

using namespace std;
using otecpp_pakkaus::pakkaa;
using otecpp_pakkaus::pura;

int main(int argc, char *argv[])
{
  string teksti(argv[1]);
  cout << "Alkuperäinen:\n" << teksti << '\n';
  vector<string> korvaukset = pakkaa(teksti);
  cout << "Tiivistetty:\n";
  for(unsigned int i = 0; i < korvaukset.size(); ++i)
  {
    cout << korvaukset[i] << '\n';
  }
  cout << teksti << '\n';
  pura(teksti, korvaukset);
  cout << "Ja taas alkuperäinen:\n" << teksti << '\n';
}
