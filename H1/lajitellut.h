#pragma once

#include <vector>
#include <string>

namespace otecpp_lajitellut
{
    std::vector<std::string> lajitellut (std::vector<std::string> mjt);
}
