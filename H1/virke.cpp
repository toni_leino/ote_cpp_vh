#include "virke.h"

#include <numeric>

namespace
{
    // Laskee ja palauttaa osamerkkijonon.
    std::string osamjono (const std::vector<std::string> &rivit,
                          size_t riv, size_t sar, size_t pit)
    {
        std::string s(pit, ' ');

        for (size_t i = 0; i < pit; ++i) {
            if (sar >= rivit[riv].length()) {
                riv++;
                if (riv >= rivit.size()) {
                    return std::string("");
                }
                sar = 0;
            }

            else {
                s[i] = rivit[riv][sar];
                sar++;
            }
        }

        return s;
    }
}

std::vector<Indeksi> otecpp_virke::etsi
        (const std::vector<std::string> &rivit, const std::string &virke)
{
    std::vector<Indeksi> indeksit;
    size_t pit = virke.length();

    for (size_t riv = 0; riv < rivit.size(); ++riv) {
        for (size_t sar = 0; sar < rivit[riv].size(); ++sar) {
            if (osamjono(rivit, riv, sar, pit) == virke) {
                indeksit.push_back(Indeksi());
                indeksit.back().push_back(riv+1);
                indeksit.back().push_back(sar+1);
            }
        }
    }

    return indeksit;
}
