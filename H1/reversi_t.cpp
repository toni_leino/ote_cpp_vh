#include <iostream>
#include <string>
#include <vector>

#include "reversi.h"

using namespace std;
using otecpp_reversi::siirrot;
using otecpp_reversi::siirto;

namespace
{
  void tulosta(const vector<string> &siirrot, const string &nimike)
  {
    cout << nimike << " mahdolliset siirrot:";
    for(unsigned int i = 0; i < siirrot.size(); ++i)
    {
      cout << " " << siirrot[i];
    }
    cout << '\n';
  }
}

int main(int argc, char *argv[])
{
  vector<string> lauta(8);
  lauta[0] = "        ";
  lauta[1] = "        ";
  lauta[2] = "        ";
  lauta[3] = "   M    ";
  lauta[4] = "   V    ";
  lauta[5] = "        ";
  lauta[6] = "        ";
  lauta[7] = "        ";
  tulosta(siirrot(lauta, false), "Pelilaudan");
  lauta[3][4] = 'V';
  lauta[4][4] = 'M';
  tulosta(siirrot(lauta, true), "Valkean");
  tulosta(siirrot(lauta, false), "Mustan");
  lauta[0] = "   M    ";
  lauta[1] = "  MVV   ";
  lauta[2] = "MVV VMM ";
  lauta[3] = "  VVM   ";
  lauta[4] = " VMMVVM ";
  lauta[5] = "M  MMV  ";
  lauta[6] = "      M ";
  lauta[7] = "        ";
  tulosta(siirrot(lauta, true), "Valkean");
  tulosta(siirrot(lauta, false), "Mustan");
  siirto(lauta, "d3", false);
  cout << "Lauta nyt\n";
  for(unsigned int i = 0; i < lauta.size(); ++i)
  {
    cout << lauta[i] << '\n';
  }
}
