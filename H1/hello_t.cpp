#include <string>
#include "hello.h"

int main(int argc, char *argv[])
{
  if(argc > 1)
  {
    std::string msg(argv[1]); // string-olio = 1. komentoriviparametri.
    otecpp_hello::hello(msg); // Kutsutaan hello-funktiotasi.
  }
}

