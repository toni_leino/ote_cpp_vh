#include "tilasto.h"

#include <algorithm> // std::sort

double otecpp_tilasto::keskiarvo (std::vector<double> luvut)
{
    double summa = 0;
    for (unsigned int i = 0; i < luvut.size(); ++i) {
        summa += luvut[i];
    }
    return summa / luvut.size();
}

double otecpp_tilasto::mediaani (std::vector<double> luvut)
{
    std::sort(luvut.begin(), luvut.end());
    std::vector<double>::size_type koko = luvut.size();
    return koko%2 ? luvut[koko/2] : (luvut[koko/2-1] + luvut[koko/2]) / 2.0;
}

double otecpp_tilasto::varianssi (std::vector<double> luvut)
{
    double ka = keskiarvo(luvut);
    std::vector<double> erotukset;
    for (unsigned int i = 0; i < luvut.size(); ++i) {
        double erotus = luvut[i] - ka;
        erotukset.push_back(erotus * erotus);
    }
    return keskiarvo(erotukset);
}
