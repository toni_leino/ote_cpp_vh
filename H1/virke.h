#pragma once

#include <vector>
#include <string>

typedef std::vector<unsigned int> Indeksi;

namespace otecpp_virke
{
    std::vector<Indeksi> etsi
            (const std::vector<std::string> &rivit, const std::string &virke);
}
