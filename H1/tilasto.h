#pragma once

#include <vector>

namespace otecpp_tilasto
{
    double keskiarvo (std::vector<double> luvut);
    double mediaani  (std::vector<double> luvut);
    double varianssi (std::vector<double> luvut);
}
