#include <iostream>
#include <string>
#include <vector>
#include "virke.h"

using namespace std;
using otecpp_virke::etsi;

namespace
{
  void tulosta(const vector< vector<unsigned int> > &kohdat, const string &virke)
  {
    cout << "Virkkeen \"" << virke << "\" esiintymät:\n";
    for(unsigned int i = 0; i < kohdat.size(); ++i)
    {
      cout << "  rivi " << kohdat[i][0] << ", sarake " << kohdat[i][1] << '\n';
    }
  }
}

int main()
{
  vector<string> rivit(5);
  rivit[0] = "yksi kaksi kolme kaksi nelja kolme";
  rivit[1] = "nelja viisi";
  rivit[2] = "kolme nelja kolme";
  rivit[3] = "kaksi kolme";
  rivit[4] = "kaksi kolmetoista";
  string virke = "kolme kaksi";
  tulosta(etsi(rivit, virke), virke);
  virke = "kolme kaksi kolme";
  tulosta(etsi(rivit, virke), virke);
}


