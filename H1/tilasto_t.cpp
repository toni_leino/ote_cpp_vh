#include <iostream>
#include <cstdlib>
#include "tilasto.h"

using namespace std;
using namespace otecpp_tilasto;

int main(int argc, char *argv[])
{
  vector<double> luvut;
  for(int i = 1; i < argc; ++i)
  {
    luvut.push_back(atof(argv[i]));
  }
  cout << "Antamiesi lukujen tunnuslukuja:\n"
       << "  keskiarvo: " << keskiarvo(luvut) << '\n'
       << "  mediaani: " << mediaani(luvut) << '\n'
       << "  varianssi: " << varianssi(luvut) << '\n';
}
