#include "hello.h"

#include <iostream>

void otecpp_hello::hello (std::string name)
{
    std::cout << "Hello " << name << "!" << std::endl;
}
