#pragma once

#include <vector>
#include <string>

namespace otecpp_pakkaus
{
    std::vector<std::string> pakkaa (std::string &teksti);
    void pura (std::string &teksti, const std::vector<std::string> &korvaukset);
}
