#include <iostream>
#include <vector>
#include "lajitellut.h"

using namespace std;
using otecpp_lajitellut::lajitellut;

int main(int argc, char *argv[])
{
  vector<string> mjt;
  for(int i = 1; i < argc; ++i)
  {
    mjt.push_back(string(argv[i]));
  }
  vector<string> mjt2 = lajitellut(mjt);
  for(int i = 0, koko = mjt2.size(); i < koko; ++i)
  {
    cout << mjt2[i] << '\n';
  }
}
