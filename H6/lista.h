#ifndef OTECPP_LISTA_H
#define OTECPP_LISTA_H

#include <iostream>
#include <vector>

namespace otecpp_lista
{
  template<typename T>
  class Lista;
}
 
namespace otecpp_lomitus
{
  template<typename T>
  void lajittele(otecpp_lista::Lista<T> &lista);
}

namespace otecpp_lista
{
  // Tarpeellista sälää
  template<typename T>
  class Lista;
  template<typename T>
  std::ostream& operator<<(std::ostream &virta, const Lista<T> &lista);

  template<typename T>
  class Solmu
  {
    T arvo;
    Solmu<T> *seur;
    
    public:
    Solmu<T>(T arvo, Solmu<T> *seur);
    T getArvo() const;
    Solmu<T> * getSeur() const;
    void setArvo(T arvo);
    void setSeur(Solmu<T> *seur);
  };
  
  template<typename T>
  class Lista
  {
    Solmu<T> *paa;
    unsigned int koko;
    
    public:
    Lista<T>();
    Lista<T>(const Lista<T> &toinen);
    ~Lista<T>();
    void lisaaEteen(T arvo);
    T poistaEdesta();
    void lisaaTaakse(T arvo);
    T poistaTakaa();
    void lisaaEteen(const Lista<T> &toinen);
    void lisaaTaakse(const Lista<T> &toinen);
    void kaanna();
    void tulosta(const std::string &nimi) const;
    unsigned int getKoko() const;

    Lista<T>& operator=(const Lista<T> &toinen);
    Lista<T>& operator+=(const Lista<T> &toinen);
    Lista<T> operator+(const Lista<T> &toinen) const;
    operator std::vector<T>() const;

    friend std::ostream& operator<< <T>(std::ostream &virta, const Lista &lista);
    friend void otecpp_lomitus::lajittele<T>(Lista &lista);

    typedef unsigned int koko_t;
  };
}

#endif
