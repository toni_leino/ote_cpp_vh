#include <cstddef>
#include <climits>
#include <iostream>

#include "lista.h"

using namespace std;

namespace otecpp_lista
{
  template<typename T>
  Solmu<T>::Solmu(T arvo, Solmu<T> *seur)
  {
    this->arvo = arvo;
    this->seur = seur;
  }

  template<typename T>
  T Solmu<T>::getArvo() const
  {
    return arvo;
  }
  
  template<typename T>
  Solmu<T> * Solmu<T>::getSeur() const
  {
    return seur;
  }

  template<typename T>
  void Solmu<T>::setArvo(T arvo)
  {
    this->arvo = arvo;
  }

  template<typename T>
  void Solmu<T>::setSeur(Solmu<T> *seur)
  {
    this->seur = seur;
  }
  
  template<typename T>
  Lista<T>::Lista()
  {
    koko = 0;
    paa = NULL;
  }

  template<typename T>
  Lista<T>::Lista(const Lista &toinen)
  {
    koko = toinen.getKoko();

    Solmu<T> *nyk = toinen.paa;
    Solmu<T> *edel = NULL;
    while (nyk) {
      Solmu<T> *tmp = new Solmu<T>(nyk->getArvo(), NULL);
      if (nyk == toinen.paa) {
        paa = tmp;
      }
      if (edel) {
        edel->setSeur(tmp);
      }
      nyk = nyk->getSeur();
      edel = tmp;
    }
  }

  template<typename T>
  Lista<T>::~Lista()
  {
    Solmu<T> *nyk = paa;
    Solmu<T> *seur = NULL;
    while (nyk) {
      seur = nyk->getSeur();
      delete nyk;
      nyk = seur;
    }
  }
  
  template<typename T>
  void Lista<T>::lisaaEteen(T arvo)
  {
    paa = new Solmu<T>(arvo, paa);
    koko += 1;
  }
    
  template<typename T>
  T Lista<T>::poistaEdesta()
  {
    T tulos = T();
    if(koko > 0)
    {
      Solmu<T> *vanha = paa;
      paa = paa->getSeur();
      koko -= 1;
      tulos = vanha->getArvo();
      delete vanha;
    }
    return tulos;
  }

  template<typename T>
  void Lista<T>::lisaaTaakse(T arvo) {
    if (paa == NULL) {
      lisaaEteen(arvo);
      return;
    }
    Solmu<T> *nyk = paa;
    while (nyk->getSeur()) {
      nyk = nyk->getSeur();
    }
    Solmu<T> *tmp = new Solmu<T>(arvo, NULL);
    nyk->setSeur(tmp);
    koko++;
  }

  template<typename T>
  T Lista<T>::poistaTakaa()
  {
    if (koko == 1) {
      T arvo = paa->getArvo();
      delete paa;
      paa = NULL;
      koko = 0;
      return arvo;
    }
    Solmu<T> *nyk = paa;
    while (nyk->getSeur()->getSeur()) {
      nyk = nyk->getSeur();
    }
    T arvo = nyk->getSeur()->getArvo();
    delete nyk->getSeur();
    nyk->setSeur(NULL);
    koko--;
    return arvo;
  }

  template<typename T>
  void Lista<T>::lisaaEteen(const Lista<T> &toinen)
  {
    Lista tmp(toinen);
    tmp.kaanna();
    
    Solmu<T> *nyk = tmp.paa;
    while (nyk) {
      lisaaEteen(nyk->getArvo());
      nyk = nyk->getSeur();
    }
  }

  template<typename T>
  void Lista<T>::lisaaTaakse(const Lista<T> &toinen)
  {
    Solmu<T> *edel = paa; // solmu, jonka taakse lisätään
    if (paa != NULL) {
        while (edel->getSeur()) {
            edel = edel->getSeur();
        }
    }
    Solmu<T> *nyk = toinen.paa; // solmu, jota ollaan kopioimassa
    koko_t n = UINT_MAX; // maksimimäärä solmuja, jotka kopioidaan
    if (&toinen == this) {
      n = koko;
    }
    while (nyk && n-- > 0) {
      Solmu<T> *tmp = new Solmu<T>(nyk->getArvo(), NULL);
      if (paa == NULL) {
          paa = tmp;
          paa->setSeur(tmp);
      } else {
          edel->setSeur(tmp);
      }
      edel = tmp;
      nyk = nyk->getSeur();
      koko++;
    }
  }

  template<typename T>
  void Lista<T>::kaanna()
  {
      Solmu<T> *s1 = NULL;
      Solmu<T> *s2 = paa;
      Solmu<T> *s3 = paa->getSeur();

      while (s3 != NULL) {
          s2->setSeur(s1);
          s1 = s2;
          s2 = s3;
          s3 = s3->getSeur();
      }
      s2->setSeur(s1);
      paa = s2;
  }

  template<typename T>
  void Lista<T>::tulosta(const string &nimi) const
  {
    cout << "Listan \"" << nimi << "\" koko on " << koko;
    if (koko == 0) {
      cout << endl;
      return;
    }
    cout << ", alkiot:";
    Solmu<T> *nyk = paa;
    while (nyk) {
      cout << " " << nyk->getArvo();
      nyk = nyk->getSeur();
    }
    cout << endl;
  }
    
  template<typename T>
  typename Lista<T>::koko_t Lista<T>::getKoko() const
  {
    return koko;
  }

  template<typename T>
  Lista<T>& Lista<T>::operator=(const Lista<T> &toinen)
  {
    if (&toinen == this) {
      return *this;
    }
    while (paa != NULL) {
      poistaEdesta();
    }
    lisaaTaakse(toinen);
    return *this;
  }

  template<typename T>
  Lista<T>& Lista<T>::operator+=(const Lista<T> &toinen)
  {
    lisaaTaakse(toinen);
    return *this;
  }

  template<typename T>
  Lista<T> Lista<T>::operator+(const Lista<T> &toinen) const
  {
    Lista<T> tmp(*this);
    tmp.lisaaTaakse(toinen);
    return tmp;
  }

  template<typename T>
  Lista<T>::operator vector<T>() const
  {
    vector<T> v;
    Solmu<T> *tmp = paa;
    while (tmp) {
      v.push_back(tmp->getArvo());
      tmp = tmp->getSeur();
    }
    return v;
  }

  template<typename T>
  ostream& operator<<(ostream &virta, const Lista<T> &lista)
  {
    Solmu<T> *tmp = lista.paa;
    while (tmp) {
      virta << tmp->getArvo();
      if ((tmp = tmp->getSeur())) {
        virta << " ";
      }
    }
    return virta;
  }
}
