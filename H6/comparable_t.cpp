#include <iostream>
#include "comparable.h"
#include "luvut.h"

using namespace std;
using otecpp_comparable::ComparableBeta;
using otecpp_comparable::Comparable;
using otecpp_luvut::Integer;
using otecpp_luvut::Murtoluku;

class OmaString : public ComparableBeta, public Comparable<OmaString>
{
  string mjono;

 public:
  OmaString(const string &mj) : mjono(mj)
  {
  }

  int compareTo(const ComparableBeta *b) const  // ComparableBeta
  {
    const OmaString *os = dynamic_cast<const OmaString*>(b);
    if (b == NULL) {
      std::cout << "Laiton vertailu!\n";
      return 0;
    } else {
      if (mjono < os->mjono) {
        return -1;
      } else if (mjono > os->mjono) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  int compareTo(const OmaString *b) const   // Comparable<OmaString>
  {
    if (mjono < b->mjono) {
      return -1;
    } else if (mjono > b->mjono) {
      return 1;
    } else {
      return 0;
    }
  }

  int compareTo(const Comparable<OmaString> *b) const  // Comparable<OmaString>
  {
    return -(b->compareTo(this));
  }
};

int main()
{
  ComparableBeta * t[3] = {new Integer(1), new OmaString("kaksi"),
                           new Integer(3)};
  Comparable<Murtoluku> * t2[3] = {new Murtoluku(1, 2), new Murtoluku(-2, 3),
                                   new Murtoluku(5, 11)};
  Comparable<OmaString> * t3[3] = {new OmaString("yksi"),
                             new OmaString("kaksi"), new OmaString("kolme")};
  for(unsigned int i = 0; i < 3; i++)
  {
    for(unsigned int j = i+1; j < 3; j++)
    {
      cout << "Indeksit " << i << " vs " << j << ": "
           << t[i]->compareTo(t[j]) << ", " << t2[i]->compareTo(t2[j])
           << " ja " << t3[i]->compareTo(t3[j]) << '\n';
    }
    delete t[i];
    delete t2[i];
    delete t3[i];
  }
}
