#include <iostream>
#include <string>

#include "lista.cpp"

using namespace std;

namespace otecpp_lista
{
  template class Lista<int>;
  template class Lista<string>;
  template ostream & operator<<(ostream &virta, const Lista<int> &lista);
  template ostream & operator<<(ostream &virta, const Lista<string> &lista);
}
