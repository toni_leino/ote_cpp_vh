#include "lomitus.h"

#include <vector>

using namespace otecpp_lista;
using std::vector;

namespace otecpp_lomitus
{
    template<typename T>
    Solmu<T> *lomita(Solmu<T> *a, Solmu<T> *b, typename Lista<T>::koko_t pit)
    {
        // uuden listan pää ja häntä
        Solmu<T> *paa = NULL;
        Solmu<T> *hanta = NULL;
        // montako solmua listoista vielä lomitetaan?
        typename Lista<T>::koko_t a_pit = pit;
        typename Lista<T>::koko_t b_pit = pit;

        while ((a_pit || b_pit) && (a != NULL || b != NULL)) {
            Solmu<T> *tmp;
            if (!a_pit || a == NULL) {
                tmp = b;
                b = b->getSeur();
                --b_pit;
            } else if (!b_pit || b == NULL) {
                tmp = a;
                a = a->getSeur();
                --a_pit;
            } else {
                if (b == NULL || a->getArvo() < b->getArvo()) {
                    tmp = a;
                    a = a->getSeur();
                    --a_pit;
                } else {
                    tmp = b;
                    b = b->getSeur();
                    --b_pit;
                }
            }
            if (!paa) {
                paa = tmp;
            } else {
                hanta->setSeur(tmp);
            }
            hanta = tmp;
            hanta->setSeur(NULL);
        }

        return paa;
    }

    template<typename T>
    void lajittele(otecpp_lista::Lista<T> &lista)
    {
        // lisätään solmut vektoriin
        std::vector<Solmu<T>*> v;
        Solmu<T> *tmp = lista.paa;
        while (tmp) {
            v.push_back(tmp);
            tmp = tmp->getSeur();
        }

        size_t pit = 1;
        while (v.size() > 1) {
            for (size_t i = 0; i+1 < v.size(); i += 2) {
                v[i] = lomita(v[i], v[i+1], pit);
            }
            std::vector<Solmu<T>*> v2;
            for (size_t i = 0; i < v.size(); i += 2) {
                v2.push_back(v[i]);
            }
            v.swap(v2);
            pit *= 2;
        }

        lista.paa = v[0];
    }
}
