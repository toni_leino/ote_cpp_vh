#include "lomitus.h"

#include <string>

using namespace otecpp_lista;
using namespace otecpp_lomitus;
using namespace std;

int main() {
    const Lista<int>::koko_t lkm = 10;
    Lista<int> lista;
    for(Lista<int>::koko_t i = 1; i <= lkm; ++i)
    {
        lista.lisaaEteen((i * 7) % 13);
    }
    cout << "Lista ennen: " << lista << endl;
    lajittele(lista);
    cout << "Lista jälkeen: " << lista << endl;
    Lista<string> lista2;
    const char * sanat[lkm] = {"yksi", "kaksi", "kolme", "neljä", "viisi"};
    for(Lista<string>::koko_t i = 0; i < lkm; ++i)
    {
        lista2.lisaaEteen(sanat[(i * 7) % 5]);
    }
    cout << "Sanalista ennen: " << lista2 << endl;
    lajittele(lista2);
    cout << "Sanalista jälkeen: " << lista2 << endl;
}
