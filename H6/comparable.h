#ifndef COMPARABLE_H
#define COMPARABLE_H

namespace otecpp_comparable
{
    class ComparableBeta
    {
        public:
        virtual ~ComparableBeta()
        {
        }
        virtual int compareTo(const ComparableBeta *b) const = 0;
    };

    template<typename T>
    class Comparable
    {
        public:
        virtual ~Comparable()
        {
        }
        virtual int compareTo(const T *b) const = 0;
        virtual int compareTo(const Comparable<T> *b) const = 0;
    };
}

#endif /* end of include guard: COMPARABLE_H */
