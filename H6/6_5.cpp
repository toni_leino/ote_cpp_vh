#include <iostream>
#include <string>

#include "lista.cpp"
#include "lomitus.cpp"

using namespace std;

namespace otecpp_lista
{
  template class Solmu<int>;
  template class Solmu<string>;
  template class Lista<int>;
  template class Lista<string>;
  template ostream & operator<<(ostream &virta, const Lista<int> &lista);
  template ostream & operator<<(ostream &virta, const Lista<string> &lista);
}

namespace otecpp_lomitus
{
  template Solmu<int> * lomita<int>(Solmu<int> *, Solmu<int> *,
                                    Lista<int>::koko_t);
  template Solmu<string> * lomita<string>(Solmu<string> *, Solmu<string> *,
                                          Lista<string>::koko_t);
  template void lajittele<int>(Lista<int> &);
  template void lajittele<string>(Lista<string> &);
}
