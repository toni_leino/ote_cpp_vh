#ifndef MADOT_H
#define MADOT_H

#include <cstddef> // size_t
#include <utility> // pair
#include <vector>

namespace otecpp_madot
{
    class MatoSimulaatio
    {
        public:
        typedef size_t koko_t;
        typedef std::vector< std::pair<koko_t, koko_t> > RuutuVektori;
        static const char RAJA  = '#';
        static const char TYHJA = ' ';

        class Mato
        {
            public:
            virtual ~Mato()
            {
            }
            char merkki() const
            {
                return merkki_;
            }
            const RuutuVektori &ruudut() const
            {
                return ruudut_;
            }
            virtual bool teeAskel(koko_t y, koko_t o, koko_t a, koko_t v) = 0;

            protected:
            RuutuVektori ruudut_;

            Mato(char merkki):
                merkki_(merkki)
            {
            }


            private:
            char merkki_;
        };

        MatoSimulaatio(koko_t korkeus, koko_t leveys);
        bool lisaaMato(Mato *mato);
        void askella(koko_t lkm);

        private:
        // Vektorissa [riv][sar] on true, jos ruudussa on madon osa.
        std::vector< std::vector<bool> > ruudut_;
        std::vector<Mato*> madot_;
    };

    class KehaMato: public MatoSimulaatio::Mato
    {
        public:
        KehaMato(char merkki,
                 MatoSimulaatio::koko_t x1, MatoSimulaatio::koko_t y1,
                 MatoSimulaatio::koko_t x2, MatoSimulaatio::koko_t y2,
                 MatoSimulaatio::koko_t pituus);
        bool teeAskel(MatoSimulaatio::koko_t y,
                      MatoSimulaatio::koko_t o,
                      MatoSimulaatio::koko_t a,
                      MatoSimulaatio::koko_t v);

        private:
        MatoSimulaatio::koko_t x1_;
        MatoSimulaatio::koko_t y1_;
        MatoSimulaatio::koko_t x2_;
        MatoSimulaatio::koko_t y2_;

        // Lisää matoon lkm osaa. Tätä käytetään rakentajassa
        void lisaaOsia(size_t lkm, size_t x, size_t y);

        // true, jos (x, y) on kulmassa
        bool kulmassa(size_t x, size_t y) const
        {
            return (x == x1_ || x == x2_) && (y == y1_ || y == y2_);
        }

        // Sijoittaa seuraavan ruudun koordinaatit annettuihin muuttujiin
        void seuraavaRuutu(size_t x, size_t y, int &dx, int &dy) const;
    };
}

#endif /* end of include guard: MADOT_H */
