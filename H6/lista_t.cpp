#include "lista.h"

using namespace std;
using namespace otecpp_lista;

int main() {
    const Lista<int>::koko_t lkm = 3;
    Lista<int> lista;
    for(Lista<int>::koko_t i = 1; i <= lkm; ++i)
    {
        lista.lisaaTaakse(i + 10);
        lista.lisaaEteen(i);
        cout << "Lista nyt: " << lista << endl;
    }
    while(lista.getKoko() > 0)
    {
        if(lista.getKoko() % 2)
        {
            lista.poistaEdesta();
        }
        else
        {
            lista.poistaTakaa();
        }
        cout << "Lista nyt: " << lista << endl;
    }
    Lista<string> lista2;
    const char * sanat[lkm] = {"yksi", "kaksi", "kolme"};
    for(Lista<string>::koko_t i = 0; i < lkm; ++i)
    {
        lista2.lisaaTaakse(sanat[i] + string("toista"));
        lista2.lisaaEteen(sanat[i]);
        cout << "Sanalista nyt: " << lista2 << endl;
    }
    while(lista2.getKoko() > 0)
    {
        if(lista2.getKoko() % 2)
        {
            lista2.poistaEdesta();
        }
        else
        {
            lista2.poistaTakaa();
        }
        cout << "Sanalista nyt: " << lista2 << endl;
    }
}
