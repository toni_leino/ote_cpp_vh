#ifndef LOMITUS_H
#define LOMITUS_H

#include <cstddef>
#include <vector>

#include "lista.h"

namespace otecpp_lomitus
{
    template<typename T>
    otecpp_lista::Solmu<T> *lomita
            (otecpp_lista::Solmu<T> *a, otecpp_lista::Solmu<T> *b,
             typename otecpp_lista::Lista<T>::koko_t pit);

    template<typename T>
    void lajittele(otecpp_lista::Lista<T> &lista);
}

#endif /* end of include guard: LOMITUS_H */
