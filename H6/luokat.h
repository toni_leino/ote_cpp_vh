#ifndef LUOKAT_H
#define LUOKAT_H

#include <iostream>

namespace otecpp_luokat
{
    class A
    {
        public:
            A(int i):
                i(i)
            {
                std::cout << "A(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~A()
            {
                std::cout << "~A(" << i << ")\n";
            }

        private:
            int i;
    };

    class B: public A
    {
        public:
            B(int i):
                A(i+1),
                i(i)
            {
                std::cout << "B(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~B()
            {
                std::cout << "~B(" << i << ")\n";
            }

        private:
            int i;
    };

    class C: public A
    {
        public:
            C(int i):
                A(i+1),
                i(i)
            {
                std::cout << "C(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~C()
            {
                std::cout << "~C(" << i << ")\n";
            }

        private:
            int i;
    };

    class D: public A
    {
        public:
            D(int i):
                A(i+1),
                i(i)
            {
                std::cout << "D(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~D()
            {
                std::cout << "~D(" << i << ")\n";
            }

        private:
            int i;
    };

    class E: public B, public C, public D
    {
        public:
            E(int i):
                B(i+1),
                C(i+1),
                D(i+1),
                i(i)
            {
                std::cout << "E(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~E()
            {
                std::cout << "~E(" << i << ")\n";
            }

        private:
            int i;
    };

    class B2: virtual public A
    {
        public:
            B2(int i):
                A(i+1),
                i(i)
            {
                std::cout << "B2(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~B2()
            {
                std::cout << "~B2(" << i << ")\n";
            }

        private:
            int i;
    };

    class C2: virtual public A
    {
        public:
            C2(int i):
                A(i+1),
                i(i)
            {
                std::cout << "C2(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~C2()
            {
                std::cout << "~C2(" << i << ")\n";
            }

        private:
            int i;
    };

    class D2: virtual public A
    {
        public:
            D2(int i):
                A(i+1),
                i(i)
            {
                std::cout << "D2(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~D2()
            {
                std::cout << "~D2(" << i << ")\n";
            }

        private:
            int i;
    };

    class E2: public B2, public C2, public D2
    {
        public:
            E2(int i):
                A(i+1),
                B2(i+1),
                C2(i+1),
                D2(i+1),
                i(i)
            {
                std::cout << "E2(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~E2()
            {
                std::cout << "~E2(" << i << ")\n";
            }

        private:
            int i;
    };

    class F: public E, public E2
    {
        public:
            F(int i):
                A(i+1),
                E(i+1),
                E2(i+1),
                i(i)
            {
                std::cout << "F(" << i << "): " << sizeof(*this) << "\n";
            }
            virtual ~F()
            {
                std::cout << "~F(" << i << ")\n";
            }

        private:
            int i;
    };
}

#endif /* end of include guard: LUOKAT_H */
