#include <cstdlib>
#include <ctime>
#include <iostream>

#include "madot.h"

using namespace std;
using otecpp_madot::MatoSimulaatio;
using otecpp_madot::KehaMato;

int main(int argc, char *argv[])
{
  int lkm = 1;
  int jakaja = 3;
  if(argc > 1)
  {
    lkm = atoi(argv[1]);
    if(argc > 2)
    {
      jakaja = atoi(argv[2]);
    }
  }
  MatoSimulaatio ms(12, 20);
  KehaMato mato('a', 3, 3, 9, 16, 6);
  KehaMato mato2('b', 2, 2, 11, 6, 5);
  KehaMato mato3('c', 11, 15, 1, 1, 10);  // Kiertää vastapäivään.
  ms.lisaaMato(&mato);
  ms.lisaaMato(&mato2);
  ms.lisaaMato(&mato3);
  for(int i = 0; i < lkm; ++i)
  {
    ms.askella(1);
    clock_t alku = clock();
    while(clock() < alku + CLOCKS_PER_SEC/jakaja)
    {  // Odottaa 1 / jakaja sekuntia
    }
  }
}
