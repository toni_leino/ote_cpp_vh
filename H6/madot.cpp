#include "madot.h"

#include <algorithm>
#include <cctype>
#include <iostream>

namespace otecpp_madot
{
    MatoSimulaatio::MatoSimulaatio(koko_t korkeus, koko_t leveys)
    {
        ruudut_.push_back(std::vector<bool>(leveys+2, true)); // yläreuna
        for (koko_t i = 0; i < korkeus; ++i) {
            ruudut_.push_back(std::vector<bool>(leveys+2, false));
            // vasen ja oikea reuna
            ruudut_.back()[0] = true;
            ruudut_.back()[leveys+1] = true;
        }
        ruudut_.push_back(std::vector<bool>(leveys+2, true)); // alareuna
    }

    bool MatoSimulaatio::lisaaMato(MatoSimulaatio::Mato *mato)
    {
        const RuutuVektori &rv = mato->ruudut();
        for (size_t i = 0; i < rv.size(); ++i) {
            const std::pair<koko_t, koko_t> &ruutu = rv[i];
            if (ruudut_[ruutu.first][ruutu.second]) {
                std::cout << "Laiton mato: ruutu varattu!\n";
                return false;
            }
        }
        madot_.push_back(mato);
        for (size_t i = 0; i < rv.size(); ++i) {
            const std::pair<koko_t, koko_t> &ruutu = rv[i];
            ruudut_[ruutu.first][ruutu.second] = true;
        }
        return true;
    }

    void MatoSimulaatio::askella(koko_t lkm)
    {
        std::vector< std::vector<char> > kentta;
recur:
        if (!lkm) return;

        // siirretään matoja
        for (size_t i = 0; i < madot_.size(); ++i) {
            // haetaan madon nykyinen pää ja häntä
            const RuutuVektori &rv = madot_[i]->ruudut();
            const std::pair<koko_t, koko_t> &paa   = rv[0];
            std::pair<koko_t, koko_t> hanta = rv[rv.size()-1];
            if (madot_[i]->teeAskel(ruudut_[paa.first-1][paa.second],
                                    ruudut_[paa.first][paa.second+1],
                                    ruudut_[paa.first+1][paa.second],
                                    ruudut_[paa.first][paa.second-1])) {
                // Ei tehdä oletusta, että vektorin muistiosoite ei muutu
                const RuutuVektori &rv2 = madot_[i]->ruudut();
                ruudut_[rv2[0].first][rv2[0].second] = true;
                ruudut_[hanta.first][hanta.second] = false;
            }
        }

        // tulostetaan kenttä
        kentta.clear();
        for (size_t riv = 0; riv < ruudut_.size(); ++riv) {
            kentta.push_back(std::vector<char>(ruudut_[riv].size(), '@'));
            for (size_t sar = 0; sar < ruudut_[riv].size(); ++sar) {
                kentta.back()[sar] = ruudut_[riv][sar] ? RAJA : TYHJA;
            }
        }
        for (size_t i = 0; i < madot_.size(); ++i) {
            char merkki = madot_[i]->merkki();
            const RuutuVektori &rv = madot_[i]->ruudut();
            for (size_t j = 0; j < rv.size(); ++j) {
                kentta[rv[j].first][rv[j].second] = j ? merkki : toupper(merkki);
            }
        }
        for (size_t i = 0; i < kentta.size(); ++i) {
            for (size_t j = 0; j < kentta[i].size(); ++j) {
                std::cout << kentta[i][j];
            }
            std::cout << "\n";
        }
        std::cout << std::endl;
        --lkm;
        goto recur;
    }

    namespace
    {
        // true, jos ja vain jos mato kulkee myötäpäivään
        bool suunta(size_t x1, size_t y1, size_t x2, size_t y2)
        {
            return x1 < x2 || y1 < y2;
        }

        // Laskee, mihin ruutuun mato lähtee kulmasta.
        // x ja y ovat nykyinen pään paikka.
        // Paluuarvossa first = dx ja second = dy.
        std::pair<int, int> seuraavaSuunta
                (size_t x1, size_t y1, size_t x2, size_t y2, size_t x, size_t y)
        {
            // true, jos mato kulkee myötäpäivään
            bool myotapaivaan = suunta(x1, y1, x2, y2);
            // vasemman yläkulman koordinaatit
            size_t kulma_x = std::min(x1, x2);
            size_t kulma_y = std::min(y1, y2);

            std::pair<int, int> suunta;

            // vasen yläkulma
            if (x == kulma_x && y == kulma_y) {
                suunta.first  = myotapaivaan ? 0 : 1;
                suunta.second = myotapaivaan ? 1 : 0;
            }
            
            // oikea yläkulma
            else if (x == kulma_x) {
                suunta.first  = myotapaivaan ? 1 :  0;
                suunta.second = myotapaivaan ? 0 : -1;
            }

            // vasen alakulma
            else if (y == kulma_y) {
                suunta.first  = myotapaivaan ? -1 : 0;
                suunta.second = myotapaivaan ?  0 : 1;
            }

            // oikea alakulma
            else {
                suunta.first  = myotapaivaan ?  0 : -1;
                suunta.second = myotapaivaan ? -1 :  0;
            }

            return suunta;
        }
    }

    KehaMato::KehaMato(char merkki,
                       MatoSimulaatio::koko_t x1, MatoSimulaatio::koko_t y1,
                       MatoSimulaatio::koko_t x2, MatoSimulaatio::koko_t y2,
                       MatoSimulaatio::koko_t pituus):
        MatoSimulaatio::Mato(merkki),
        x1_(x1),
        y1_(y1),
        x2_(x2),
        y2_(y2)
    {
        lisaaOsia(pituus, x1, y1);
    }

    bool KehaMato::teeAskel(MatoSimulaatio::koko_t y,
                            MatoSimulaatio::koko_t o,
                            MatoSimulaatio::koko_t a,
                            MatoSimulaatio::koko_t v)
    {
        const std::pair<size_t, size_t> &paa = ruudut_[0];
        int dx, dy;
        seuraavaRuutu(paa.first, paa.second, dx, dy);

        if ((dx == -1 && dy ==  0 && y == 0)
         || (dx ==  1 && dy ==  0 && a == 0)
         || (dx ==  0 && dy == -1 && v == 0)
         || (dx ==  0 && dy ==  1 && o == 0)) {
            ruudut_.pop_back();
            std::pair<size_t, size_t> tmp(paa.first + dx, paa.second + dy);
            ruudut_.insert(ruudut_.begin(), tmp);
            return true;
        }
        return false;
    }

    void KehaMato::lisaaOsia(size_t lkm, size_t x, size_t y)
    {
        if (lkm == 0) {
            return;
        }
        
        int dy, dx;
        seuraavaRuutu(x, y, dx, dy);

        lisaaOsia(lkm-1, x + dx, y + dy);

        std::pair<size_t, size_t> ruutu(x, y);
        ruudut_.push_back(ruutu);
    }

    void KehaMato::seuraavaRuutu(size_t x, size_t y, int &dx, int &dy) const
    {
        // true, jos mato liikkuu myötäpäivään
        bool myotapaivaan = suunta(x1_, y1_, x2_, y2_);
        // vasemman yläkulman koordinaatit
        size_t kulma_x = std::min(x1_, x2_);
        size_t kulma_y = std::min(y1_, y2_);
        // Lasketaan seuraava ruutu (rivejä säästetty)
        if (kulmassa(x, y)) {
            std::pair<int, int> suunta = seuraavaSuunta(x1_, y1_, x2_, y2_, x, y);
            dx = suunta.first;
            dy = suunta.second;
        } else {
            dx = ((y == y1_ || y == y2_) ? 1 : 0) * ((myotapaivaan == (y != kulma_y)) ? 1 : -1);
            dy = ((x == x1_ || x == x2_) ? 1 : 0) * ((myotapaivaan == (x == kulma_x)) ? 1 : -1);
        }
    }
}
